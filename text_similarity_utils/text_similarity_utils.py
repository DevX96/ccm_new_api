import numpy as np
from elasticsearch import Elasticsearch
import time

from embeddings_generator.embeddings_request import get_embeddings
from embeddings_generator.preprocess_text import preprocess_description
import settings
from nltk.tokenize import sent_tokenize
import string

def clean_text(text):
    text = text.lower()   
    text = re.sub("'", '', text)
    text = re.sub("[^\w\s\.]", " ", text)
    text = re.sub(" \d+", " ", text)
    text = re.sub(' +', ' ', text)
    text = text.strip()
   
    return text

def text_checker(sent):
    new_sent = []
   
    checkers = ['formerly known','headquartered','subsidiary of','based in','founded in']
    for sent_ in sent:
        c = 0
        for check in checkers:
            if check in sent_:
                c =1
        if c == 0:
            new_sent.append(sent_)
    return new_sent


def preprocess_description_v2(text):
    print('V2')
    
    proc_text = clean_text(text)    
    proc_text = text_checker(sent_tokenize(proc_text))
    proc_text = " ".join([i for i in proc_text])

    punc_remove = proc_text.translate(str.maketrans("","", string.punctuation))
    digit_remove = re.sub(r'\d+', '', punc_remove)
    non_asc_remove = ''.join([i if ord(i)<128 else '' for i in digit_remove])
    cleaned = ' '.join(non_asc_remove.split()).lower()
    

    return cleaned


def preprocess_description(text):

    punc_remove = text.translate(str.maketrans("","", string.punctuation))
    digit_remove = re.sub(r'\d+', '', punc_remove)
    non_asc_remove = ''.join([i if ord(i)<128 else '' for i in digit_remove])
    cleaned = ' '.join(non_asc_remove.split()).lower()
    

    return cleaned

class TextSimilarityUtils:


    def __init__(self, sub_desc):

        self.sub_desc = sub_desc

    
    def get_bm25(self, es_object, desc_col_name="single_summary", gics_filters=None, size=100):
        
        # if gics_filters is not None:
        #     must_query = [{
        #                         "match": 
        #                             {desc_col_name: self.sub_desc}
        #                     }
        #                 ]
        #     filter_query = {"bool": {"must": [
        #                         {"bool": {"should": [{"match": gics_filter} for gics_filter in gics_filters]
        #                         }}
        #                     ]}}
        #     must_query.extend(filter_query)
            
        #     es_query = {
        #         "query":{
        #             "bool": {
        #                 "must": must_query
                            
        #             }
        #         }
        #     }
        # else:
        #     es_query = {
        #         "query":{
        #             "match": {desc_col_name: self.sub_desc}
        #         }
        #     }
        if gics_filters is None:
            es_query = {
                "query":{
                    "match": {desc_col_name: self.sub_desc}
                }
            }
        else:
            es_query = {
                "query":{
                    "bool": {
                        "must": [
                            {"match": {desc_col_name: self.sub_desc}}, 
                            gics_filters]
                            
                    }
                }
            }    
        
        print(es_query)

        t0 = time.time()

        es_results = es_object.query(es_query, size=size)

        t1 = time.time()
        print("Time taken for elastic search querying: ", t1 - t0)

        return es_results["hits"]["hits"]


    def get_faiss_similarity(self, fs_object, sub_embedding, size=10000):

        sim_uuids, dist = fs_object.get_neighbor(sub_embedding, num_neighbor=size)

        return sim_uuids, dist

    
    def compute_l2_distances(self, X, Y):

        dists = -2 * np.dot(Y, X.T) + np.sum(X**2, axis=1) + np.sum(Y**2, axis=1)[:, np.newaxis]
        
        return dists**(1/2)

    def piecewise_similarity(self, distance):

        if distance <= 3.5:

            percentage = (1/1.05)**(2*distance)

        else:

            percentage = np.exp(1-distance/2.6)

        return percentage*100


    def compute_cosine_similarity(self, X, Y):

        den = np.sqrt(np.einsum('ij,ij->i',Y,Y)*np.einsum('j,j',X,X))

        out = Y.dot(X)/den

        print(out.shape)

        return out


    def argsort_dist(self, dist_array, top_n = 10, ascending=False):

        if ascending:
            return np.argpartition(dist_array, range(top_n))[:top_n]
        
        else:
            return np.argpartition(-dist_array, range(top_n))[:top_n]

    def generate_emb(self, desc, embedding_shape=1024, gpu=None, embedding_type="bert"):
        emb = get_embeddings(text=preprocess_description(desc), secret_key=settings.EMBEDDING_SECRET_KEY, embedding_type=embedding_type)
        print(type(emb))
        print(len(emb))
        return np.reshape(np.array(emb, dtype=np.float32), (1, embedding_shape))


if __name__ == "__main__":

    
    pass