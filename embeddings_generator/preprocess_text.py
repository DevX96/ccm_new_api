import string
import re
import nltk
from nltk.tokenize import sent_tokenize


def clean_text(text):
    text = text.lower()   
    text = re.sub("'", '', text)
    text = re.sub("[^\w\s\.]", " ", text)
    text = re.sub(" \d+", " ", text)
    text = re.sub(' +', ' ', text)
    text = text.strip()
   
    return text

def text_checker(sent):
    new_sent = []
   
    checkers = ['formerly known','headquartered','subsidiary of','based in','founded in']
    for sent_ in sent:
        c = 0
        for check in checkers:
            if check in sent_:
                c =1
        if c == 0:
            new_sent.append(sent_)
    return new_sent


def preprocess_description(text):
    print('V2')
    
    proc_text = clean_text(text)    
    proc_text = text_checker(sent_tokenize(proc_text))
    proc_text = " ".join([i for i in proc_text])

    punc_remove = proc_text.translate(str.maketrans("","", string.punctuation))
    digit_remove = re.sub(r'\d+', '', punc_remove)
    non_asc_remove = ''.join([i if ord(i)<128 else '' for i in digit_remove])
    cleaned = ' '.join(non_asc_remove.split()).lower()
    
    return cleaned

def preprocess_description_old(text):

    punc_remove = text.translate(str.maketrans("","", string.punctuation))
    digit_remove = re.sub(r'\d+', '', punc_remove)
    non_asc_remove = ''.join([i if ord(i)<128 else '' for i in digit_remove])
    cleaned = ' '.join(non_asc_remove.split()).lower()
    

    return cleaned



if __name__ == "__main__":

    text = "Six Flags Entertainment Corporation is the worldâ€™s largest regional theme park company with $1.1 billion in revenue and 18 parks across North America. The company operates 16 parks in the United States, one in Mexico City and one in Montreal, Canada. For more than 53 years, Six Flags has entertained millions of families with world-class coasters, themed rides, thrilling water parks and unique attractions including up-close animal encounters, Fright FestÂ® and Holiday in the ParkÂ®"

    cleaned_text = preprocess_description(text)

    print(cleaned_text)