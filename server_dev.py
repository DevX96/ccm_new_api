from flask import Flask,jsonify,request,Response
from flask_cors import CORS
import json
import traceback
import sys
import settings
import os
import hashlib
import numpy as np
from elasticsearch import Elasticsearch
import cProfile
import logging
import time

from settings import *

app = Flask(__name__)
CORS(app)


def check_for_secret_id(request_data):
    
    try:
        if 'secret_id' not in request_data.keys():
            return False, "Secret Key Not Found."
        
        else:
            if request_data['secret_id'] == "@P@>w]128-.>12]'jRq120y|ap.:HF.)Bqawetgagbfshqwetqegh":
                return True, "Secret Key Matched"
            else:
                return False, "Secret Key Does Not Match. Incorrect Key."
    except Exception as e:
        message = "Error while checking secret id: " + str(e)
        return False,message

############################### IMPORT CUSTOM PACKAGES ############################

# print("IMPORTING FAISS INDEX HANDLER")

# from text_similarity_utils.faiss_search import FaissSearch

print ("IMPORTING TEXT SIMILARITY HANDLER")

from similarity_comparision_files.compare_similarity_sent_transformer_handler import handle_sentence_transformer_similarity

print ("IMPORTING MODEL HANDLER")

from model_inference.keras_model_73 import Models73
from model_inference.gics_multi_label_handler import handle_gics_multilabel_prediction

print ("IMPORTING EMBEDDINGS HANDLER")

from embeddings_generator.embeddings_request import get_embeddings

print ("IMPORTING MNN HANDLER")

from mnn_files.mnn_competitor_handler import handle_mnn_competitor_similarity

print ("IMPORTING BATCH TEXT SIMILARITY HANDLER")

from similarity_comparision_files.batch_text_similarity_handler import handle_batch_text_similarity

print ("Importing Similar Companies Sentence Transformers Handler V5.0 (V4 + Manual GICS L4 Groups)")

from similarity_comparision_files.similarity_handler_sentence_transformers_v5_0 import handle_similarity_request_sent_trans_v5_0

print ("Importing Similar Companies Sentence Transformers Handler V5.1 (V4 + GICS L3 Based GICS L4 Groups)")

from similarity_comparision_files.similarity_handler_sentence_transformers_v5_1 import handle_similarity_request_sent_trans_v5_1

print("Initializing LinkedIn Similar Tags Handler")

from linkedin_tag_similarity.find_similar_tags_handler import handle_linkedin_tags_similarity

print ("Importing Direct Market Size Extraction Handler")

from market_size_files.direct_market_size_handler import handle_direct_market_size_request

print ("Importing Multi Market Size Extraction Handler")

from market_size_files.multi_ner_handler_celery import handle_multi_ner_request

print ("Importing Crawling and Scrapping Handler")
from crawling_and_scrapping_handler import handle_crawling_and_scrapping_request,handle_scrapping_request

print("Importing Earning Call Get One Handler")
from ect_get_one_handler import handler_ect_get_one

print("Importing Earning Call Get Multiple Handler")
from ect_get_multiple_handler import handler_ect_get_multiple

print("Importing News Topic Modeling Handler")
from news_topic_modeling_handler import get_news_topic_modeling

print("Importing XIRR Handler")
from xirr_handler import handler_xirr_analysis

print ("Importing Similar Companies Sentence Transformers Handler V6.0 (V4 + Manual GICS L4 Groups)")

from similarity_comparision_files.similarity_handler_sentence_transformers_v6_0 import handle_similarity_request_sent_trans_v6_0

print ("Importing Similar Companies Sentence Transformers Handler V6.1 (V4 + Manual GICS L4 Groups)")

from similarity_comparision_files.similarity_handler_sentence_transformers_v6_1 import handle_similarity_request_sent_trans_v6_1

################################ ELASTIC SEARCH VARIABLES ###########################

# elastic_seach_crendtials = np.load(settings.ELASTIC_SEARCH_CREDENTIALS_PATH, allow_pickle=True).item()

# username_cipher_suite = Fernet(elastic_seach_crendtials["username"]["encrypt_key"])
# password_cipher_suite = Fernet(elastic_seach_crendtials["password"]["encrypt_key"])


# elk_search = Elasticsearch(['40.68.5.140'], 
#                         http_auth=(str(username_cipher_suite.decrypt(elastic_seach_crendtials["username"]["cipher_text"]).decode()), \
#                             str(password_cipher_suite.decrypt(elastic_seach_crendtials["password"]["cipher_text"]).decode())),
#                         scheme="http",
#                         port=9200,
#                         retry_on_timeout=True)

# del elastic_seach_crendtials
# del username_cipher_suite
# del password

prod_search = Elasticsearch(['40.68.5.140'], 
                        http_auth=("elastic", \
                            "73Elas@Login#$20"),
                        scheme="http",
                        port=9200,
                        retry_on_timeout=True)

elk_search = Elasticsearch(['23.102.26.210'], 
                        http_auth=("elastic", \
                            "73Elk@Uar2020"),
                        scheme="http",
                        port=9200,
                        retry_on_timeout=True)


################################ FAISS VARIABLES ###########################

# fs_object = FaissSearch(settings.FAISS_INDEX_PATH, settings.FAISS_UUIDS_PATH)
# fs_uuids = np.load(settings.FAISS_UUIDS_PATH)

############################################################################

############################ GICS L4 MODEL ###############################


gics_model = Models73(classes_path=settings.GICS_L4_LABEL_PATH, weight_path=settings.GICS_L4_MODEL_PATH)

gics_group_dict = np.load(settings.GICS_L4_GROUP_PATH, allow_pickle=True).item()

############################ Multilabel MODEL ###############################


multi_label_model = Models73(classes_path=settings.MULTI_LABEL_CLASS_PATH, weight_path=settings.MULTI_LABEL_MODEL_PATH)

#########################################################################

###########################################################
# New Text Similarity
###########################################################

@app.route('/new_text_similarity',methods=['POST'])

def new_text_similarity():

    global elk_search

    try:

        #GET JSON DATA
        request_data = request.get_json()
        print ("Request Data: ", request_data)
        
        #CHECK FOR SECRET ID
        secret_id_status,secret_id_message = check_for_secret_id(request_data)
        print ("Secret ID Check: ", secret_id_status,secret_id_message)
        if not secret_id_status:
            data = {}
            data['message'] = secret_id_message
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=401, mimetype='application/json')      
            return resp
        
        #PROCESS TEXT AND RETURN RESPONSE
        if 'data' in request_data.keys():
            working_data = request_data['data']
            data = {}
            temp = []
            for wd in working_data:

                if "gics_tag" in wd.keys():
                    gics_tag = wd["gics_tag"]
                    if gics_tag.lower() == "true":
                        gics_tag = True
                else:
                    gics_tag = True

                if ("gics_class" in wd.keys()) and (wd["gics_class"] is not None):
                    gics_label = wd["gics_class"].strip().upper().replace(" ", "_")
                else:
                    gics_label = None
                

                if "source_db" in wd.keys():
                    source_db = wd["source_db"]
                else:
                    source_db = None

                if "top_n" in wd.keys():
                    top_n = wd["top_n"]
                else:
                    top_n = 100

                if ('text' not in wd.keys()) and ("uuid" not in wd.keys()):
                    continue
                else:

                    if "text" in wd.keys():
                        text = wd["text"]
                    elif "uuid" in wd.keys():
                        query = {
                                    "query":{
                                        "match": {settings.UNIFIED_UUID_FIELD+".keyword": wd["uuid"]}
                                    }
                                }
                        _ = elk_search.search(index=settings.UNIFIED_INDEX, body=query)["hits"]["hits"][0]["_source"]
                        text = _[settings.UNIFIED_DESCRIPTION_FIELD]
                        
                        if gics_tag and gics_label is None:
                            gics_label = _[settings.UNIFIED_GICS_FIELD].strip().upper().replace(" ", "_")

                        print(text)
                    else:
                        print("Enter valid text or uuid")
                        text = None

                    if gics_tag and gics_label is None:
                        text_embs = get_embeddings(text=text, secret_key=settings.EMBEDDING_SECRET_KEY)
                        text_embs = np.reshape(np.array(text_embs), (1, 1024))

                        gics_label = gics_model.predict_classes(text_embs, batch_size=1, top_n=1)[0][0][0]
                    # else:
                    #     gics_label = None

                    
                    processed_data = handle_sentence_transformer_similarity(elk_search, text, gics_group_dict=gics_group_dict,\
                                         gics_tag=gics_label, source_db=source_db, top_n=top_n)

                    if "uuid" in wd.keys():
                        try:
                            uuid_idx = processed_data.index(wd["uuid"])
                            processed_data.pop(uuid_idx)
                            del uuid_idx

                        except Exception as e:
                            print("UUID not in procecessed data list: ", e)

                    temp_dict = {}
                    temp_dict['text'] = text
                    temp_dict["gics_tag"] = gics_tag
                    temp_dict["gics_label"] = gics_label
                    temp_dict["source_db"] = source_db
                    temp_dict['processed_data'] = processed_data
                    temp.append(temp_dict)
            
            data['complete_processed_data'] = temp
            data['success'] = True
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=200, mimetype='application/json')
            return resp
        else:
            data = {}
            data['message'] = "Invalid Request. Data Not Found For Processing."
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=400, mimetype='application/json')
            return resp

    except Exception as e:
        traceback.print_exc()
        data = {}
        data['message'] = "Error While Processing NEW TEXT SIMILARITY Request: " + str(e)
        data['success'] = False
        try:
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
        except Exception as e:
            data['request_id'] = None
        data = json.dumps(data)
        resp = Response(data, status=500, mimetype='application/json')
        return resp


###########################################################
# MNN Similarity
###########################################################

@app.route('/new_mnn_similarity',methods=['POST'])

def new_mnn_similarity():

    global elk_search

    try:

        #GET JSON DATA
        request_data = request.get_json()
        print ("Request Data: ", request_data)
        
        #CHECK FOR SECRET ID
        secret_id_status,secret_id_message = check_for_secret_id(request_data)
        print ("Secret ID Check: ", secret_id_status,secret_id_message)
        if not secret_id_status:
            data = {}
            data['message'] = secret_id_message
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=401, mimetype='application/json')      
            return resp
        
        #PROCESS TEXT AND RETURN RESPONSE
        if 'data' in request_data.keys():
            working_data = request_data['data']
            data = {}
            temp = []
            for wd in working_data:

                if "priority_order" in wd.keys():
                    priority_order = wd["priority_order"]
                else:
                    priority_order = None

                if "source_company_desc" in wd.keys():
                    desc = wd["source_company_desc"]
                else:
                    desc = None 

                if "source_company_country" in wd.keys():
                    country = wd["source_company_country"]
                else:
                    country = None 

                if "source_company_employee_count" in wd.keys():
                    employee_count = wd["source_company_employee_count"]
                else:
                    employee_count = None 

                if 'source_company_uuid' not in wd.keys():
                    continue
                else:
                    _id = wd['source_company_uuid']

                    processed_data, gics_label, priority_order = handle_mnn_competitor_similarity(_id, priority_order, employee_count, desc, \
                                                                                    country, elk_search, gics_group_dict=gics_group_dict)
                    
                    temp_dict = {}
                    temp_dict['source_company_uuid'] = _id
                    temp_dict['priority_order'] = priority_order
                    temp_dict["gics_label"] = gics_label
                    temp_dict['processed_data'] = processed_data
                temp.append(temp_dict)
            
            data['complete_processed_data'] = temp
            data['success'] = True
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=200, mimetype='application/json')
            return resp
        else:
            data = {}
            data['message'] = "Invalid Request. Data Not Found For Processing."
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=400, mimetype='application/json')
            return resp

    except Exception as e:
        traceback.print_exc()
        data = {}
        data['message'] = "Error While Processing NEW MNN SIMILARITY Request: " + str(e)
        data['success'] = False
        try:
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
        except Exception as e:
            data['request_id'] = None
        data = json.dumps(data)
        resp = Response(data, status=500, mimetype='application/json')
        return resp

###########################################################
# GICS L4 Prediction
###########################################################

@app.route('/gics_node4_classification',methods=['POST'])
def gics_node4_classification():

    try:

        #GET JSON DATA
        request_data = request.get_json()
        print ("Request Data: ", request_data)
        
        #CHECK FOR SECRET ID
        secret_id_status,secret_id_message = check_for_secret_id(request_data)
        print ("Secret ID Check: ", secret_id_status,secret_id_message)
        if not secret_id_status:
            data = {}
            data['message'] = secret_id_message
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=401, mimetype='application/json')      
            return resp
        
        #PROCESS TEXT AND RETURN RESPONSE
        if 'data' in request_data.keys():
            working_data = request_data['data']
            data = {}
            temp = []
            for wd in working_data:
                if 'id' not in wd.keys() or 'text' not in wd.keys():
                    continue
                else:
                    _id = wd['id']
                    text = wd['text']
                    processed_data = handle_gics_multilabel_prediction(gics_model, text, top_n=1)
                    temp_dict = {}
                    temp_dict['id'] = _id
                    temp_dict['text'] = text
                    temp_dict['processed_data'] = processed_data
                temp.append(temp_dict)
            
            data['complete_processed_data'] = temp
            data['success'] = True
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=200, mimetype='application/json')
            return resp
        else:
            data = {}
            data['message'] = "Invalid Request. Data Not Found For Processing."
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=400, mimetype='application/json')
            return resp

    except Exception as e:
        traceback.print_exc()
        data = {}
        data['message'] = "Error While Processing GICS Node 4 Classification Request: " + str(e)
        data['success'] = False
        try:
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
        except Exception as e:
            data['request_id'] = None
        data = json.dumps(data)
        resp = Response(data, status=500, mimetype='application/json')
        return resp

###########################################################
# MULTI LABEL Prediction
###########################################################

@app.route('/multi_label_classification',methods=['POST'])
def multi_label_classification():

    try:

        #GET JSON DATA
        request_data = request.get_json()
        print ("Request Data: ", request_data)
        
        #CHECK FOR SECRET ID
        secret_id_status,secret_id_message = check_for_secret_id(request_data)
        print ("Secret ID Check: ", secret_id_status,secret_id_message)
        if not secret_id_status:
            data = {}
            data['message'] = secret_id_message
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=401, mimetype='application/json')      
            return resp
        
        #PROCESS TEXT AND RETURN RESPONSE
        if 'data' in request_data.keys():
            working_data = request_data['data']
            data = {}
            temp = []
            for wd in working_data:
                if 'id' not in wd.keys() or 'text' not in wd.keys():
                    continue
                else:
                    _id = wd['id']
                    text = wd['text']
                    processed_data = handle_gics_multilabel_prediction(multi_label_model, text, top_n=5)
                    temp_dict = {}
                    temp_dict['id'] = _id
                    temp_dict['text'] = text
                    temp_dict['processed_data'] = processed_data
                temp.append(temp_dict)
            
            data['complete_processed_data'] = temp
            data['success'] = True
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=200, mimetype='application/json')
            return resp
        else:
            data = {}
            data['message'] = "Invalid Request. Data Not Found For Processing."
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=400, mimetype='application/json')
            return resp

    except Exception as e:
        traceback.print_exc()
        data = {}
        data['message'] = "Error While Processing MULTI LABEL Classification Request: " + str(e)
        data['success'] = False
        try:
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
        except Exception as e:
            data['request_id'] = None
        data = json.dumps(data)
        resp = Response(data, status=500, mimetype='application/json')
        return resp

###########################################################################
# Sentence Transformers Similarity API V5.0 (V4 + Manual GICS L4 Groups)
###########################################################################

@app.route('/similar_companies_sent_trans_v5_0',methods=['POST'])
def company_similarity_sent_trans_v5_0():
    global gics_model, multi_label_model, prod_search

    try:

        #GET JSON DATA
        request_data = request.get_json()
        print ("Request Data: ", request_data)
        
        #CHECK FOR SECRET ID
        secret_id_status,secret_id_message = check_for_secret_id(request_data)
        print ("Secret ID Check: ", secret_id_status,secret_id_message)
        if not secret_id_status:
            data = {}
            data['message'] = secret_id_message
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=401, mimetype='application/json')      
            return resp     
        
        #PROCESS TEXT AND RETURN RESPONSE
        if 'data' in request_data.keys():
            working_data = request_data['data']
            data = {}
            temp = []
            for wd in working_data:
                if 'id' not in wd.keys() or 'text' not in wd.keys():
                    continue
                else:
                    _id = wd['id']
                    text = wd['text']
                    domain = None
                    gics_l4 = None
                    sent=True
                    cap=True
                    sent_cap = False

                    country = None

                    if 'valuation_date' in wd.keys():
                        valuation_date = wd['valuation_date']
                    else:
                        valuation_date = None
                    
                    if 'domain' in wd.keys():
                        domain = wd['domain']

                    if 'country' in wd.keys():
                        country = wd['country']

                    if 'sent' in wd.keys():
                        sent = wd['sent']    
                        if sent.lower() == 'true':
                            sent = True
                        else:
                            sent = False
                    if 'cap' in wd.keys():
                        cap = wd['cap']    
                        if cap.lower() == 'true':
                            cap = True
                        else:
                            cap = False

                    if 'sent_cap' in wd.keys():
                        sent_cap = wd['sent_cap']    
                        if sent_cap.lower() == 'true':
                            sent = True
                            cap = True

                            
                    if 'gics_l4' in wd.keys():
                        if wd['gics_l4'].lower() != "none":
                            gics_l4 = wd['gics_l4']
                    else:
                        gics_l4 = None                    
                    

                    text_embs = get_embeddings(text=text, secret_key=settings.EMBEDDING_SECRET_KEY)
                    text_embs = np.reshape(np.array(text_embs), (1, 1024))

                    gics_pred = gics_model.predict_classes(text_embs, batch_size=1, top_n=1)[0][0][0]

                    if gics_l4 is not None:

                        gics_l4_processed = "_".join(gics_l4.upper().split(" "))

                        gics_pred = gics_l4_processed
                    
                    processed_data = handle_similarity_request_sent_trans_v5_0(text, domain, multi_label_model, prod_search, elk_search, gics_group_dict, sent=sent, cap=cap, gics_pred=gics_pred, country = country, valuation_date = valuation_date)
                    temp_dict = {}
                    temp_dict['id'] = _id
                    temp_dict['text'] = text
                    temp_dict['processed_data'] = processed_data
                temp.append(temp_dict)
            
            data['complete_processed_data'] = temp
            data['success'] = True
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=200, mimetype='application/json')
            return resp
        else:
            data = {}
            data['message'] = "Invalid Request. Data Not Found For Processing."
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=400, mimetype='application/json')
            return resp

    except Exception as e:
        traceback.print_exc()
        data = {}
        data['message'] = "Error While Processing Similarity Company Request for Sentence Transformers v5_0: " + str(e)
        data['success'] = False
        try:
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
        except Exception as e:
            data['request_id'] = None
        data = json.dumps(data)
        resp = Response(data, status=500, mimetype='application/json')
        return resp

###########################################################################
# Sentence Transformers Similarity API V5.1 (V4 + L3 Based GICS L4 Groups)
###########################################################################

@app.route('/similar_companies_sent_trans_v5_1',methods=['POST'])
def company_similarity_sent_trans_v5_1():
    global gics_model, multi_label_model, prod_search

    try:

        #GET JSON DATA
        request_data = request.get_json()
        print ("Request Data: ", request_data)
        
        #CHECK FOR SECRET ID
        secret_id_status,secret_id_message = check_for_secret_id(request_data)
        print ("Secret ID Check: ", secret_id_status,secret_id_message)
        if not secret_id_status:
            data = {}
            data['message'] = secret_id_message
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=401, mimetype='application/json')      
            return resp     
        
        #PROCESS TEXT AND RETURN RESPONSE
        if 'data' in request_data.keys():
            working_data = request_data['data']
            data = {}
            temp = []
            for wd in working_data:
                if 'id' not in wd.keys() or 'text' not in wd.keys():
                    continue
                else:
                    _id = wd['id']
                    text = wd['text']
                    gics_l4 = None
                    sent=True
                    cap=True
                    sent_cap = False
                    domain = None

                    country = None

                    if 'valuation_date' in wd.keys():
                        valuation_date = wd['valuation_date']
                    else:
                        valuation_date = None
                    
                    if 'domain' in wd.keys():
                        domain = wd['domain']

                    if 'country' in wd.keys():
                        country = wd['country']

                    if 'sent' in wd.keys():
                        sent = wd['sent']    
                        if sent.lower() == 'true':
                            sent = True
                        else:
                            sent = False
                    if 'cap' in wd.keys():
                        cap = wd['cap']    
                        if cap.lower() == 'true':
                            cap = True
                        else:
                            cap = False

                    if 'sent_cap' in wd.keys():
                        sent_cap = wd['sent_cap']    
                        if sent_cap.lower() == 'true':
                            sent = True
                            cap = True

                            
                    if 'gics_l4' in wd.keys():
                        if wd['gics_l4'].lower() != "none":
                            gics_l4 = wd['gics_l4']
                    else:
                        gics_l4 = None                    
                    

                    text_embs = get_embeddings(text=text, secret_key=settings.EMBEDDING_SECRET_KEY)
                    text_embs = np.reshape(np.array(text_embs), (1, 1024))

                    gics_pred = gics_model.predict_classes(text_embs, batch_size=1, top_n=1)[0][0][0]

                    if gics_l4 is not None:

                        gics_l4_processed = "_".join(gics_l4.upper().split(" "))

                        gics_pred = gics_l4_processed
                    
                    processed_data = handle_similarity_request_sent_trans_v5_1(text, domain, multi_label_model, prod_search, elk_search, gics_group_dict, sent=sent, cap=cap, gics_pred=gics_pred, country = country, valuation_date = valuation_date)
                    temp_dict = {}
                    temp_dict['id'] = _id
                    temp_dict['text'] = text
                    temp_dict['processed_data'] = processed_data
                temp.append(temp_dict)
            
            data['complete_processed_data'] = temp
            data['success'] = True
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=200, mimetype='application/json')
            return resp
        else:
            data = {}
            data['message'] = "Invalid Request. Data Not Found For Processing."
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=400, mimetype='application/json')
            return resp

    except Exception as e:
        traceback.print_exc()
        data = {}
        data['message'] = "Error While Processing Similarity Company Request for Sentence Transformers v5_1: " + str(e)
        data['success'] = False
        try:
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
        except Exception as e:
            data['request_id'] = None
        data = json.dumps(data)
        resp = Response(data, status=500, mimetype='application/json')
        return resp


##########################################################################
# LINKEDIN TAGS SIMILARITY
##########################################################################

@app.route('/linkedin_tags_similarity',methods=['POST'])
def get_linkedin_tags_similarity():
    global prod_search, elk_search

    try:

        request_data = request.get_json()

        #CHECK FOR SECRET ID
        secret_id_status,secret_id_message = check_for_secret_id(request_data)
        print ("Secret ID Check: ", secret_id_status,secret_id_message)
        if not secret_id_status:
            data = {}
            data['message'] = secret_id_message
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=401, mimetype='application/json')
            return resp
        
        #PROCESS TEXT AND RETURN RESPONSE
        if 'tag' in request_data.keys():
            tag = request_data['tag']
            print ("tag: ",tag)

            if 'top_n' in request_data.keys():
                top_n = request_data['top_n']
            
            else:
                top_n = 10

            data = {}
            temp = handle_linkedin_tags_similarity(tag, top_n, es_specialty=prod_search, es_cooccur=elk_search)
            data['similar_tags'] = temp
            data['success'] = True
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=200, mimetype='application/json')
            return resp
        else:
            data = {}
            data['message'] = "Invalid Request. Tag Not Found For Processing."
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=400, mimetype='application/json')
            return resp

    except Exception as e:
        traceback.print_exc()
        data = {}
        data['message'] = "Error While Processing LinkedIn Tags Request: " + str(e)
        data['success'] = False
        try:
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
        except Exception as e:
            print (e)
            data['request_id'] = None
        data = json.dumps(data)
        resp = Response(data, status=500, mimetype='application/json')
        return resp


##########################################################################
# DIRECT MARKET SIZE EXTRACTOR
##########################################################################

@app.route('/direct_market_size',methods=['POST'])
def direct_market_size():

    try:

        request_data = request.get_json()

        #CHECK FOR SECRET ID
        secret_id_status,secret_id_message = check_for_secret_id(request_data)
        print ("Secret ID Check: ", secret_id_status,secret_id_message)
        if not secret_id_status:
            data = {}
            data['message'] = secret_id_message
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=401, mimetype='application/json')
            return resp
        
        #PROCESS TEXT AND RETURN RESPONSE
        if 'tags_list' in request_data.keys():
            tags_list = request_data['tags_list']
            print ("tags_list: ",tags_list)

            data = {}
            temp = handle_direct_market_size_request(tags_list)
            data['response'] = temp
            
            data = json.dumps(data)
            resp = Response(data, status=200, mimetype='application/json')
            return resp
        else:
            data = {}
            data['message'] = "Invalid Request. Tags List Not Found For Processing."
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=400, mimetype='application/json')
            return resp

    except Exception as e:
        traceback.print_exc()
        data = {}
        data['message'] = "Error While Processing Direct Market Size Request: " + str(e)
        data['success'] = False
        try:
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
        except Exception as e:
            print (e)
            data['request_id'] = None
        data = json.dumps(data)
        resp = Response(data, status=500, mimetype='application/json')
        return resp


##########################################################################
# MULTI MARKET SIZE EXTRACTOR
##########################################################################

@app.route('/multi_market_size_extraction',methods=['POST'])
def multi_market_size_extraction():

    try:

        request_data = request.get_json()

        #CHECK FOR SECRET ID
        secret_id_status,secret_id_message = check_for_secret_id(request_data)
        print ("Secret ID Check: ", secret_id_status,secret_id_message)
        if not secret_id_status:
            data = {}
            data['message'] = secret_id_message
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=401, mimetype='application/json')
            return resp
        
        #PROCESS TEXT AND RETURN RESPONSE
        if 'data' in request_data.keys():
            working_data = request_data['data']
            data = {}
            temp = []
            for wd in working_data:
                if 'url' not in wd.keys() or 'text' not in wd.keys():
                    temp_dict = {}
                    temp_dict['processed_data'] = None
                else:
                    if 'market_name' not in wd.keys():
                        market_name = None
                    else:
                        market_name = wd['market_name']

                    if 'publish_date' not in wd.keys():
                        publish_date = None
                    else:
                        publish_date = wd['publish_date']

                    _id = wd['url']
                    text = wd['text']
                    processed_data = handle_multi_ner_request(text,market_name=market_name,publish_date=publish_date)
                    temp_dict = {}
                    temp_dict['url'] = _id
                    temp_dict['market_name'] = market_name
                    temp_dict['text'] = text
                    temp_dict['processed_data'] = processed_data
                temp.append(temp_dict)
            
            data['complete_processed_data'] = temp
            data['success'] = True
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=200, mimetype='application/json')
            return resp
        else:
            data = {}
            data['message'] = "Invalid Request. Text Not Found For Processing."
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=400, mimetype='application/json')
            return resp

    except Exception as e:
        traceback.print_exc()
        data = {}
        data['message'] = "Error While Processing Market Size Extraction Request: " + str(e)
        data['success'] = False
        try:
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
        except Exception as e:
            print (e)
            data['request_id'] = None
        data = json.dumps(data)
        resp = Response(data, status=500, mimetype='application/json')
        return resp


##########################################################################
#SCRAPPING and CRAWLING
##########################################################################

@app.route('/scrape_urls',methods=['POST'])
def scrape_urls():

    try:

        request_data = request.get_json()

        #CHECK FOR SECRET ID
        secret_id_status,secret_id_message = check_for_secret_id(request_data)
        print ("Secret ID Check: ", secret_id_status,secret_id_message)
        if not secret_id_status:
            data = {}
            data['message'] = secret_id_message
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=401, mimetype='application/json')
            return resp
        
        #PROCESS TEXT AND RETURN RESPONSE
        if 'data' in request_data.keys():
            working_data = request_data['data']
            data = {}
            if 'mode' in request_data.keys(): mode = request_data["mode"]
            else: mode = "DIFFBOT"
            temp = handle_scrapping_request(working_data, mode=mode)
            data['complete_processed_data'] = temp
            data['success'] = True
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=200, mimetype='application/json')
            return resp
        else:
            data = {}
            data['message'] = "Invalid Request. Data Not Found For Processing."
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=400, mimetype='application/json')
            return resp

    except Exception as e:
        traceback.print_exc()
        data = {}
        data['message'] = "Error While Processing Scrapping Request: " + str(e)
        data['success'] = False
        try:
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
        except Exception as e:
            print (e)
            data['request_id'] = None
        data = json.dumps(data)
        resp = Response(data, status=500, mimetype='application/json')
        return resp


##########################################################################
#News Topic Modeling
##########################################################################

@app.route('/news_topic_modeling',methods=['POST'])
def news_topic_modeling():

    try:

        #GET JSON DATA
        request_data = request.get_json()
        print ("Request Data: ", request_data)
        
        #CHECK FOR SECRET ID
        secret_id_status,secret_id_message = check_for_secret_id(request_data)
        print ("Secret ID Check: ", secret_id_status,secret_id_message)
        if not secret_id_status:
            data = {}
            data['message'] = secret_id_message
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=401, mimetype='application/json')      
            return resp
        
        #PROCESS TEXT AND RETURN RESPONSE
        if 'data' in request_data.keys():
            working_data = request_data['data']
            data = {}
            temp = []

            for wd in working_data:
                if "urls" not in wd.keys():
                    # continue
                    urls = []
                else:
                    urls = wd["urls"]

                    if "company_name" in wd.keys():
                        company_name = wd["company_name"]
                    else:
                        # company_name = ""
                        continue

                    logging.info("Extracting news info from url")
                    tic = time.time()

                    formatted_result = get_news_topic_modeling(urls, company_name)
                    
                    logging.info(f"Time taken to run topic modeling : {time.time()-tic}")
                    
                    temp.append({"processed_data": formatted_result})
            data['complete_processed_data'] = temp
            data['success'] = True
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=200, mimetype='application/json')
            return resp
        else:
            data = {}
            data['message'] = "Invalid Request. Data Not Found For Processing."
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=400, mimetype='application/json')
            return resp

    except Exception as e:
        traceback.print_exc()
        data = {}
        data['message'] = "Error While Processing NEWS TOPIC MODELING Request: " + str(e)
        data['success'] = False
        try:
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
        except Exception as e:
            data['request_id'] = None
        data = json.dumps(data)
        resp = Response(data, status=500, mimetype='application/json')
        return resp

##########################################################################
#XIRR analysis
##########################################################################

@app.route('/xirr_analysis',methods=['POST'])
def xirr_analysis():

    try:

        #GET JSON DATA
        request_data = request.get_json()
        print ("Request Data: ", request_data)
        
        #CHECK FOR SECRET ID
        secret_id_status,secret_id_message = check_for_secret_id(request_data)
        print ("Secret ID Check: ", secret_id_status,secret_id_message)
        if not secret_id_status:
            data = {}
            data['message'] = secret_id_message
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=401, mimetype='application/json')      
            return resp
        
        #PROCESS TEXT AND RETURN RESPONSE
        if 'data' in request_data.keys():
            working_data = request_data['data']
            data = {}
            temp = handler_xirr_analysis(working_data)
            
            data['complete_processed_data'] = temp
            data['success'] = True
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=200, mimetype='application/json')
            return resp
        else:
            data = {}
            data['message'] = "Invalid Request. Data Not Found For Processing."
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=400, mimetype='application/json')
            return resp

    except Exception as e:
        traceback.print_exc()
        data = {}
        data['message'] = "Error While Processing XIRR analysis Request: " + str(e)
        data['success'] = False
        try:
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
        except Exception as e:
            data['request_id'] = None
        data = json.dumps(data)
        resp = Response(data, status=500, mimetype='application/json')
        return resp


##########################################################################
# ECT Get One
##########################################################################

@app.route('/ect_getone',methods=['POST'])
def ect_getone():

    try:

        #GET JSON DATA
        request_data = request.get_json()
        print ("Request Data: ", request_data)
        
        #CHECK FOR SECRET ID
        secret_id_status,secret_id_message = check_for_secret_id(request_data)
        print ("Secret ID Check: ", secret_id_status,secret_id_message)
        if not secret_id_status:
            data = {}
            data['message'] = secret_id_message
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=401, mimetype='application/json')      
            return resp
        
        #PROCESS TEXT AND RETURN RESPONSE
        if 'data' in request_data.keys():
            working_data = request_data['data']
            data = {}
            temp = []

            for wd in working_data:
                if "ticker" not in wd.keys():
                    continue
                if "quarter" not in wd.keys():
                    continue
                else:
                    ticker = wd["ticker"]
                    quarter = wd["quarter"]

                    result = handler_ect_get_one(ticker, quarter)
                    
                    temp.append({"processed_data": result})
            data['complete_processed_data'] = temp
            data['success'] = True
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=200, mimetype='application/json')
            return resp
        else:
            data = {}
            data['message'] = "Invalid Request. Data Not Found For Processing."
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=400, mimetype='application/json')
            return resp

    except Exception as e:
        traceback.print_exc()
        data = {}
        data['message'] = "Error While Processing ECT GET ONE Request: " + str(e)
        data['success'] = False
        try:
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
        except Exception as e:
            data['request_id'] = None
        data = json.dumps(data)
        resp = Response(data, status=500, mimetype='application/json')
        return resp

##########################################################################
# ECT Get MULTIPLE
##########################################################################

@app.route('/ect_getmultiple',methods=['POST'])
def ect_getmultiple():

    try:

        #GET JSON DATA
        request_data = request.get_json()
        print ("Request Data: ", request_data)
        
        #CHECK FOR SECRET ID
        secret_id_status,secret_id_message = check_for_secret_id(request_data)
        print ("Secret ID Check: ", secret_id_status,secret_id_message)
        if not secret_id_status:
            data = {}
            data['message'] = secret_id_message
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=401, mimetype='application/json')      
            return resp
        
        #PROCESS TEXT AND RETURN RESPONSE
        if 'data' in request_data.keys():
            working_data = request_data['data']
            data = {}
            temp = []

            for wd in working_data:
                if "ticker" not in wd.keys():
                    continue
                if "quarter" not in wd.keys():
                    continue
                if "lookback_quarters" not in wd.keys():
                    continue
                else:
                    ticker = wd["ticker"]
                    quarter = wd["quarter"]
                    lookback_quarters = wd["lookback_quarters"]

                    result = handler_ect_get_multiple(ticker, quarter, lookback_quarters)
                    
                    temp.append({"processed_data": result})
            data['complete_processed_data'] = temp
            data['success'] = True
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=200, mimetype='application/json')
            return resp
        else:
            data = {}
            data['message'] = "Invalid Request. Data Not Found For Processing."
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=400, mimetype='application/json')
            return resp

    except Exception as e:
        traceback.print_exc()
        data = {}
        data['message'] = "Error While Processing ECT GET MULTIPLE Request: " + str(e)
        data['success'] = False
        try:
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
        except Exception as e:
            data['request_id'] = None
        data = json.dumps(data)
        resp = Response(data, status=500, mimetype='application/json')
        return resp

###########################################################################
# Sentence Transformers Similarity API V6.0 (V4 + Manual GICS L4 Groups + CAPIQ CCM CTM)
###########################################################################


############################### IMPORT CUSTOM PACKAGES FOR CCM_NEW ############################
#from similarity_handler import handle_similarity_request
from similarity_handler_v2 import handle_similarity_request

############### GICS GROUP ######################
gics_group_dict = json.load(open(settings.GICS_L4_GROUP_PATH))

def convert_gics(form_gics):
    form_gics = form_gics.upper()
    form_gics = form_gics.replace('AND', '&')
    form_gics = form_gics.replace(' ', '_')
    return form_gics

import ast

@app.route('/similar_companies_sent_trans_v6_0',methods=['POST'])
def company_similarity_sent_trans_v6_0():
    global gics_model, multi_label_model, prod_search

    try:

        #GET JSON DATA
        request_data = request.get_json()
        print ("Request Data: ", request_data)
        
        #CHECK FOR SECRET ID
        secret_id_status,secret_id_message = check_for_secret_id(request_data)
        print ("Secret ID Check: ", secret_id_status,secret_id_message)
        if not secret_id_status:
            data = {}
            data['message'] = secret_id_message
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=401, mimetype='application/json')      
            return resp     
        
        #PROCESS TEXT AND RETURN RESPONSE
        if 'data' in request_data.keys():
            working_data = request_data['data']
            data = {}
            temp = []
            for wd in working_data:
                if 'id' not in wd.keys() or 'text' not in wd.keys():
                    continue
                else:
                    _id = wd['id']
                    text = wd['text']
                    gics_l4 = None
                    sent=True
                    cap=True
                    sent_cap = False

                    domain = None

                    country = None

                    if 'valuation_date' in wd.keys():
                        valuation_date = wd['valuation_date']
                    else:
                        valuation_date = None
                    
                    if 'domain' in wd.keys():
                        domain = wd['domain']

                    if 'country' in wd.keys():
                        country = wd['country']

                    if 'sent' in wd.keys():
                        sent = wd['sent']    
                        if sent.lower() == 'true':
                            sent = True
                        else:
                            sent = False
                    if 'cap' in wd.keys():
                        cap = wd['cap']    
                        if cap.lower() == 'true':
                            cap = True
                        else:
                            cap = False

                    if 'sent_cap' in wd.keys():
                        sent_cap = wd['sent_cap']    
                        if sent_cap.lower() == 'true':
                            sent = True
                            cap = True
                    
                    mnn = True
                    if 'mnn' in wd.keys():
                        if wd['mnn'].lower()  == "false":
                            mnn=False
                    
                    ml_tags = None

                    if 'multilabel_tags' in wd.keys():
                        ml_tags_temp = ast.literal_eval(wd['multilabel_tags'])
                        if (len(ml_tags_temp) == 5 and type(ml_tags_temp) == list):
                            ml_tags = ml_tags_temp


                            
                    if 'gics_l4' in wd.keys():
                        if wd['gics_l4'].lower() != "none":
                            gics_l4 = wd['gics_l4']
                    else:
                        gics_l4 = None                    
                    

                    text_embs = get_embeddings(text=text, secret_key=settings.EMBEDDING_SECRET_KEY)
                    text_embs = np.reshape(np.array(text_embs), (1, 1024))

                    gics_pred = gics_model.predict_classes(text_embs, batch_size=1, top_n=1)[0][0][0]

                    if gics_l4 is not None:

                        #gics_l4_processed = "_".join(gics_l4.upper().split(" "))
                        gics_l4_processed = convert_gics(gics_l4)

                        gics_pred = gics_l4_processed
                    
                    processed_data = handle_similarity_request_sent_trans_v6_0(text, domain, multi_label_model, elk_search, elk_search,
                                                                 gics_group_dict, sent=sent, cap=cap, gics_pred=gics_pred, country = country,
                                                                 valuation_date = valuation_date,mnn=mnn,ml_tags=ml_tags)
                    temp_dict = {}
                    temp_dict['id'] = _id
                    temp_dict['text'] = text
                    temp_dict['processed_data'] = processed_data
                temp.append(temp_dict)
            
            data['complete_processed_data'] = temp
            data['success'] = True
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=200, mimetype='application/json')
            return resp
        else:
            data = {}
            data['message'] = "Invalid Request. Data Not Found For Processing."
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=400, mimetype='application/json')
            return resp

    except Exception as e:
        traceback.print_exc()
        data = {}
        data['message'] = "Error While Processing Similarity Company Request for Sentence Transformers v6_0: " + str(e)
        data['success'] = False
        try:
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
        except Exception as e:
            data['request_id'] = None
        data = json.dumps(data)
        resp = Response(data, status=500, mimetype='application/json')
        return resp

###########################################################################
# Sentence Transformers Similarity API V6.1 (V4 + Manual GICS L4 Groups + CAPIQ CCM CTM)
###########################################################################

@app.route('/similar_companies_sent_trans_v6_1',methods=['POST'])
def company_similarity_sent_trans_v6_1():
    global gics_model, multi_label_model, prod_search

    try:

        #GET JSON DATA
        request_data = request.get_json()
        print ("Request Data: ", request_data)
        
        #CHECK FOR SECRET ID
        secret_id_status,secret_id_message = check_for_secret_id(request_data)
        print ("Secret ID Check: ", secret_id_status,secret_id_message)
        if not secret_id_status:
            data = {}
            data['message'] = secret_id_message
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=401, mimetype='application/json')      
            return resp     
        
        #PROCESS TEXT AND RETURN RESPONSE
        if 'data' in request_data.keys():
            working_data = request_data['data']
            data = {}
            temp = []
            for wd in working_data:
                if 'id' not in wd.keys() or 'text' not in wd.keys():
                    continue
                else:
                    _id = wd['id']
                    text = wd['text']
                    gics_l4 = None
                    sent=True
                    cap=True
                    sent_cap = False

                    domain = None

                    country = None

                    if 'valuation_date' in wd.keys():
                        valuation_date = wd['valuation_date']
                    else:
                        valuation_date = None
                    
                    if 'domain' in wd.keys():
                        domain = wd['domain']

                    if 'country' in wd.keys():
                        country = wd['country']

                    if 'sent' in wd.keys():
                        sent = wd['sent']    
                        if sent.lower() == 'true':
                            sent = True
                        else:
                            sent = False
                    if 'cap' in wd.keys():
                        cap = wd['cap']    
                        if cap.lower() == 'true':
                            cap = True
                        else:
                            cap = False

                    if 'sent_cap' in wd.keys():
                        sent_cap = wd['sent_cap']    
                        if sent_cap.lower() == 'true':
                            sent = True
                            cap = True

                            
                    if 'gics_l4' in wd.keys():
                        if wd['gics_l4'].lower() != "none":
                            gics_l4 = wd['gics_l4']
                    else:
                        gics_l4 = None                    
                    

                    text_embs = get_embeddings(text=text, secret_key=settings.EMBEDDING_SECRET_KEY)
                    text_embs = np.reshape(np.array(text_embs), (1, 1024))

                    gics_pred = gics_model.predict_classes(text_embs, batch_size=1, top_n=1)[0][0][0]

                    if gics_l4 is not None:

                        gics_l4_processed = "_".join(gics_l4.upper().split(" "))

                        gics_pred = gics_l4_processed
                    
                    processed_data = handle_similarity_request_sent_trans_v6_1(text, domain, multi_label_model, elk_search, elk_search, gics_group_dict, sent=sent, cap=cap, gics_pred=gics_pred, country = country, valuation_date = valuation_date)
                    temp_dict = {}
                    temp_dict['id'] = _id
                    temp_dict['text'] = text
                    temp_dict['processed_data'] = processed_data
                temp.append(temp_dict)
            
            data['complete_processed_data'] = temp
            data['success'] = True
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=200, mimetype='application/json')
            return resp
        else:
            data = {}
            data['message'] = "Invalid Request. Data Not Found For Processing."
            data['success'] = False
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
            data = json.dumps(data)
            resp = Response(data, status=400, mimetype='application/json')
            return resp

    except Exception as e:
        traceback.print_exc()
        data = {}
        data['message'] = "Error While Processing Similarity Company Request for Sentence Transformers v6_0: " + str(e)
        data['success'] = False
        try:
            if 'request_id' in request_data.keys():
                data['request_id'] = request_data['request_id']
            else:
                data['request_id'] = None
        except Exception as e:
            data['request_id'] = None
        data = json.dumps(data)
        resp = Response(data, status=500, mimetype='application/json')
        return resp


if __name__ == "__main__":
    
    app.run(host="0.0.0.0",port=9000,use_reloader=False,debug=True,threaded=True)