import os
import sys
import numpy as np

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
print(BASE_DIR)

######################MODULES LIST FOR IMPORTING##########################
MODULES = [ "",
            "elastic_search",
            "embedding_generation",
            "similarity_comparision_files",
            "embeddings_generator",
            "model_inference",
            "mnn_files",
            "linkedin_tag_similarity",
            "market_size_files",
            "serp_files",
            "topic_modeling",
            "xirr_api",
            "text_similarity_utils"
        ]

for module in MODULES:
    sys.path.append(os.path.join(BASE_DIR,module))
##########################################################################


########################### FAISS FILES ###############################

# FAISS_INDEX_NAME = "new_73strings/sent_unified_db_faiss_embedding.index"
# FAISS_INDEX_PATH = os.path.join("/".join(BASE_DIR.split("/")[:-1]), FAISS_INDEX_NAME)

# FAISS_UUIDS_NAME = "new_73strings/sent_unified_db_db_ids.npy"
# FAISS_UUIDS_PATH = os.path.join("/".join(BASE_DIR.split("/")[:-1]), FAISS_UUIDS_NAME)

#################Google Custom search API########################
GOOGLE_API_KEY = "AIzaSyDzfDQ9SceRzQ_qq82GmdkeVw8Osj02mnA"
GOOGLE_CSE_ID = "006598532973645258840:tjvzwg9qota"

################## Elastic Search Credentials ############################

ELASTIC_SEARCH_CREDENTIALS_NAME = "elastic_search_credentials.npy"
ELASTIC_SEARCH_CREDENTIALS_PATH = os.path.join("/".join(BASE_DIR.split("/")[:-1]), ELASTIC_SEARCH_CREDENTIALS_NAME)

########################CELERY INFO###########################

#--->MARKET SIZE
MARKET_SIZE_CELERY_NAME = 'multi_market_size'
MARKET_SIZE_CELERY_BROKER = 'amqp://localhost'
MARKET_SIZE_CELERY_BACKEND = 'amqp://localhost'

#################### EMBEDDING SERVER ########################################

EMBEDDING_IP = "http://0.0.0.0:9200/"
EMBEDDING_INDEX = "embedding_generator"

EMBEDDING_SECRET_KEY = "@P@>w]128-.>12]'jRq120y|ap.:HF.)Bqawetgagbfsh"

#################### GICS L4 MODEL ########################################

MODEL_DB_PATH = "model_db"

GICS_L4_MODEL_NAME = "gics_weights_68.hdf5"
GICS_L4_MODEL_PATH = os.path.join(BASE_DIR, MODEL_DB_PATH, GICS_L4_MODEL_NAME)

GICS_L4_LABEL_NAME = "gics4_tag_classes.npy"
GICS_L4_LABEL_PATH = os.path.join(BASE_DIR, MODEL_DB_PATH, GICS_L4_LABEL_NAME)

GICS_L4_GROUP_NAME = "gics_l4_groups_ccm_v2.json"
GICS_L4_GROUP_PATH = os.path.join(BASE_DIR, MODEL_DB_PATH, GICS_L4_GROUP_NAME)

IND_GICS_MAP_FILE_NAME = "ind_gics_map.json"
IND_GICS_MAP_PATH = os.path.join(BASE_DIR, MODEL_DB_PATH, IND_GICS_MAP_FILE_NAME)

'''
GICS_L4_GROUP_NAME = "gics_l4_groups_ccm.npy"
GICS_L4_GROUP_PATH = os.path.join(BASE_DIR, MODEL_DB_PATH, GICS_L4_GROUP_NAME)
'''
#---> MULTI LABEL MODEL

MULTI_LABEL_MODEL_NAME = "multi_label_weight_27.hdf5"
MULTI_LABEL_MODEL_PATH = os.path.join(BASE_DIR, MODEL_DB_PATH, MULTI_LABEL_MODEL_NAME)

MULTI_LABEL_CLASS_NAME = "multi_label_tag_classes.npy"
MULTI_LABEL_CLASS_PATH = os.path.join(BASE_DIR, MODEL_DB_PATH, MULTI_LABEL_CLASS_NAME)

#----> Sentieo Descs

SENTEIO_DESCS_FILE_NAME = "senteio_data_without_pp.pkl"
SENTEIO_DESCS_PATH = os.path.join(BASE_DIR, MODEL_DB_PATH, SENTEIO_DESCS_FILE_NAME)

#----> CapIQ Descs

CAPIQ_DESCS_FILE_NAME = "capiq_data_pp60k_1808.pkl"
CAPIQ_DESCS_PATH = os.path.join(BASE_DIR, MODEL_DB_PATH, CAPIQ_DESCS_FILE_NAME)

#---> Direct Market Size
DIRECT_MARKET_SIZE_FILE_PATH = "direct_market_size/final_updated_data.csv"
DIRECT_MARKET_SIZE_FILE = os.path.join(BASE_DIR, MODEL_DB_PATH, DIRECT_MARKET_SIZE_FILE_PATH)

########################### UNIFIED DB VARIABLES ###########################################

#--->UNIFIED DB

# UNIFIED_INDEX = "unified_intermediate"

# UNIFIED_UUID_FIELD = "id"
# UNIFIED_COMPANY_NAME_FIELD = "final_company_name"
# UNIFIED_GICS_FIELD = "final_gics_industry"
# UNIFIED_MULTI_LABEL_FIELD = "final_multilabel_classification_tags"
# UNIFIED_SOURCE_DB_FIELD = "source"
# UNIFIED_DESCRIPTION_FIELD = "final_description"
# UNIFIED_COUNTRY_FIELD = "location_country"
# UNIFIED_EMPLOYEE_COUNT_FIELD = "final_employee_size"

UNIFIED_INDEX = "unified_master"

UNIFIED_UUID_FIELD = "id"
UNIFIED_COMPANY_NAME_FIELD = "company_name"
UNIFIED_GICS_FIELD = "gics_industry"
UNIFIED_MULTI_LABEL_FIELD = "multilabel_classification_tags"
UNIFIED_SOURCE_DB_FIELD = "source"
UNIFIED_DESCRIPTION_FIELD = "description"
UNIFIED_COUNTRY_FIELD = "location_country"
UNIFIED_EMPLOYEE_COUNT_FIELD = "employee_size"
UNIFIED_USD_CUNDING_FIELD = "funding_in_usd"

#---> UNIFIED SENTECE TRANSFORMER

UNIFIED_SENT_EMB_INDEX = "unified_sentence_transformer_embeddings"

UNIFIED_SENT_EMB_UUID_FIELD = "uuid"
UNIFIED_SENT_EMB_EMBEDDING_FIELD = "embedding"

########################## SENTEIO DB VARIABLES ##################################

SENTEIO_INDEX = "sentieo_excel_upload"

SENTEIO_UUID_FIELD = "ticker"
SENTEIO_DESCRIPTION_FIELD = "description"

########################## CAPIQ DB VARIABLES ##################################

CAPIQ_INDEX = "transactions_capiq_data"

CAPIQ_UUID_FIELD = "transid"
CAPIQ_DESCRIPTION_FIELD = "business_description_target_to_issuer"
CAPIQ_VALUATION_FIELD = "transaction_announced_date"

########################### LINKEDIN SPECIALTY DB VARIABLES #####################

LINKEDIN_SPECIALTY_INDEX = "li_specialities"

LINKEDIN_SPECIALTY_FIELD = "speciality"

########################### LINKEDIN COOCCURRENCE DB VARIABLES #####################

LINKEDIN_COOCCUR_INDEX = "linkedin_cooccuring_tags_test"

LINKEDIN_COOCCUR_NAME_FIELD = "linkedin_tags"
LINKEDIN_COOCCUR_COUNT_FIELD = "tag_occurance"
LINKEDIN_COOCCUR_CO_TAGS_FIELD = "co_occuring_tags"


########################### MNN Variables ###########################################

MNN_COLUMN_DICT = {
    UNIFIED_UUID_FIELD: "uuid",
    UNIFIED_COMPANY_NAME_FIELD: "company_name",
    UNIFIED_MULTI_LABEL_FIELD: "new_priority_list",
    UNIFIED_COUNTRY_FIELD: "country",
    UNIFIED_EMPLOYEE_COUNT_FIELD: "employee_count",
    UNIFIED_DESCRIPTION_FIELD: "desc"
}


################ SENTENCE TRANSFORMERS SIMILARITY API FILES ###########

SIMILARITY_FILES_BASE_DIR = os.path.join("/".join(BASE_DIR.split("/")[:-1]), 'similarity_api_files')

SENTEIO_EMBEDDINGS_SENTENCE_TRANSFORMERS_NAME = "senteio_embs_preprocessed_sentence_transformers.npy"
SENTEIO_EMBEDDINGS_SENTENCE_TRANSFORMERS_PATH = os.path.join(SIMILARITY_FILES_BASE_DIR,SENTEIO_EMBEDDINGS_SENTENCE_TRANSFORMERS_NAME)

SENTEIO_IDS_SENTENCE_TRANSFORMERS_NAME = "senteio_tickers_preprocessed_sentence_transformers.npy"
SENTEIO_IDS_SENTENCE_TRANSFORMERS_PATH = os.path.join(SIMILARITY_FILES_BASE_DIR,SENTEIO_IDS_SENTENCE_TRANSFORMERS_NAME)

SENTEIO_IDS_GICS_DICT_NAME = "senteio_gics_dict.npy"
SENTEIO_IDS_GICS_DICT_PATH = os.path.join(SIMILARITY_FILES_BASE_DIR,SENTEIO_IDS_GICS_DICT_NAME)

CAPIQ_EMBEDDINGS_SENTENCE_TRANSFORMERS_NAME = "capiq_embs_sentence_transformers.npy"
CAPIQ_EMBEDDINGS_SENTENCE_TRANSFORMERS_PATH = os.path.join(SIMILARITY_FILES_BASE_DIR,CAPIQ_EMBEDDINGS_SENTENCE_TRANSFORMERS_NAME)

CAPIQ_IDS_SENTENCE_TRANSFORMERS_NAME = "capiq_transids_sentence_transformers.npy"
CAPIQ_IDS_SENTENCE_TRANSFORMERS_PATH = os.path.join(SIMILARITY_FILES_BASE_DIR,CAPIQ_IDS_SENTENCE_TRANSFORMERS_NAME)

CAPIQ_IDS_GICS_DICT_NAME = "capiq_gics_dict.npy"
CAPIQ_IDS_GICS_DICT_PATH = os.path.join(SIMILARITY_FILES_BASE_DIR,CAPIQ_IDS_GICS_DICT_NAME)

CAPIQ_CCM_SENTENCE_TRANSFORMERS_EMBS_NAME = "capiq_embs_sent.npy"
CAPIQ_CCM_SENTENCE_TRANSFORMERS_EMBS_PATH = os.path.join(SIMILARITY_FILES_BASE_DIR,CAPIQ_CCM_SENTENCE_TRANSFORMERS_EMBS_NAME)

CAPIQ_CCM_SENTENCE_TRANSFORMERS_UUID_NAME = "capiq_uuid_sent.npy"
CAPIQ_CCM_SENTENCE_TRANSFORMERS_UUID_PATH = os.path.join(SIMILARITY_FILES_BASE_DIR,CAPIQ_CCM_SENTENCE_TRANSFORMERS_UUID_NAME)

CAPIQ_CTM_EMB_DB_NAME = "capiq_ctm_level_db"
CAPIQ_CTM_EMB_DB_PATH = os.path.join(SIMILARITY_FILES_BASE_DIR,CAPIQ_CTM_EMB_DB_NAME)

CAPIQ_CTM_EMB_DTYPE_NAME = "capiq_ctm_level_db/embedding_dtype.pkl"
CAPIQ_CTM_EMB_DTYPE_PATH = os.path.join(SIMILARITY_FILES_BASE_DIR,CAPIQ_CTM_EMB_DTYPE_NAME)

########################### LINKEDIN SIMILAR TAGS API #####################
LINKEDIN_TAGS_CO_OCCURRENCE_BASE_DIR = os.path.join("/".join(BASE_DIR.split("/")[:-1]), "linkedin_tags_similarity")

LINKEDIN_CO_OCCURRECE_MODEL_NAME = "crawl-300d-2M-subword.bin"
LINKEDIN_CO_OCCURRECE_MODEL_PATH = os.path.join(LINKEDIN_TAGS_CO_OCCURRENCE_BASE_DIR, LINKEDIN_CO_OCCURRECE_MODEL_NAME)

LINKEDIN_CO_OCCRENRRENCE_VECTOR_NAME = "en_li_tags_with_ft_embs_v2_polyglot.npy" 
LINKEDIN_CO_OCCRENRRENCE_VECTOR_PATH = os.path.join(LINKEDIN_TAGS_CO_OCCURRENCE_BASE_DIR, LINKEDIN_CO_OCCRENRRENCE_VECTOR_NAME)

LINKEDIN_HIGH_OCCRENRRENCE_SPECIALTIES_NAME = "high_occuring_li_specialties.txt" 
LINKEDIN_HIGH_OCCRENRRENCE_SPECIALTIES_PATH = os.path.join(LINKEDIN_TAGS_CO_OCCURRENCE_BASE_DIR, LINKEDIN_HIGH_OCCRENRRENCE_SPECIALTIES_NAME)


########################### NEW CAPIQ CCM DB VARIABLE #####################################

CAPIQ_CCM_INDEX = "company_master_data_with_industry_and_status"

CAPIQ_CCM_UUID_FIELD = "company_id"
CAPIQ_CCM_DESC_FIELD = "description"
CAPIQ_CCM_GICS_FIELD = "gics_tag"
CAPIQ_CCM_MARKET_CAP_FIELD = "market_cap"
CAPIQ_CCM_MARKET_COUNTRY_FIELD = "country"

############################ NEW CAPIQ DATA ################################################

CAPIQ_CTM_INDEX = "transaction_data_buyer_data_gics"

CAPIQ_CTM_UUID_FIELD = "transaction_id"
CAPIQ_CTM_DESC_FIELD = "target_company_description"
CAPIQ_CTM_GICS_FIELD = "gics_tag"
CAPIQ_CTM_TRANSACTION_DATE_FIELD = "transaction_announced_date"


######################### CAPIQ CCM VARIABLES #####################################

CAPIQ_CCM_EMB_INDEX = "capiq_ccm_sent_embeddings"

CAPIQ_CCM_EMB_UUID_FIELD = "capiq_uuid"
CAPIQ_CCM_EMB_EMBEDDING_FIELD = "embedding"

######################### CAPIQ CTM EMBEDDINGS VARIABLES #####################################

CAPIQ_CTM_EMB_INDEX = "ctm_sent_embeddings"

CAPIQ_CTM_EMB_UUID_FIELD = "capiq_uuid"
CAPIQ_CTM_EMB_EMBEDDING_FIELD = "embedding"

