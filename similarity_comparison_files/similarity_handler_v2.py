from compare_similarity_v2 import Similarity
from elastic_search.elastic_search_processing import ElasticServer
from apollo_api import get_specialties_and_industry
from text_similarity_utils.text_similarity_utils import TextSimilarityUtils

import json
import numpy as np
import pandas as pd
import time
import settings
from datetime import datetime


#senteio_emb = settings.CAPIQ_CCM_SENTENCE_TRANSFORMERS_EMBS_PATH
#senteio_ticker = settings.CAPIQ_CCM_SENTENCE_TRANSFORMERS_UUID_PATH


with open(settings.LINKEDIN_HIGH_OCCRENRRENCE_SPECIALTIES_PATH) as li_specialities:

    li_specialities = li_specialities.readlines()

li_specialities = [i.replace("\n", "").strip() for i in li_specialities]


def handle_similarity_request(text, domain, multi_label_pred, elk_server, li_server, gics_groups_dict, sent=True, cap=True, gics_pred=None, country = None, valuation_date = None,mnn=True,ml_tags=None):

    # es_sentieo = ElasticServer(elk_server, index=settings.SENTEIO_INDEX)

    # es_capiq = ElasticServer(elk_server, index=settings.CAPIQ_INDEX)

    es_cooccur = ElasticServer(li_server, index=settings.LINKEDIN_COOCCUR_INDEX)

    #_similarity = Similarity(senteio_emb, senteio_ticker, gics_groups_dict, elk_server)
    _similarity = Similarity(gics_groups_dict, elk_server)
    

    ts_util = TextSimilarityUtils(text)

    #########################
    # Get BM25 results from ES
    #########################

    #Senteio

    # results = ts_util.get_bm25(es_sentieo, desc_col_name=settings.SENTEIO_DESCRIPTION_FIELD, size=500)
    # similar_ticker_list_senteio = [i['_source'][settings.SENTEIO_UUID_FIELD] for i in results]

    # #CapIQ

    # results = ts_util.get_bm25(es_capiq, desc_col_name=settings.CAPIQ_DESCRIPTION_FIELD, size=500)

    # if valuation_date is None:

    #     similar_ticker_list_capiq = [i['_source']['transid'] for i in results]

    # else:
    #     similar_ticker_list_capiq = []

    #     valuation_datetime = datetime.strptime(valuation_date, '%Y-%m-%d')

    #     for i in results:
    #         datetime_i = datetime.strptime(i['_source']['transaction_announced_date'].split("T")[0], '%Y-%m-%d')
    #         if datetime_i > valuation_datetime:
    #             continue
    #         similar_ticker_list_capiq.append(i['_source']['transid'])

    ######################
    # Get Sentence Transformer Embeddings
    ######################
    print('V2')
    a = time.time()
    #text = preprocess_description(text)
    emb = ts_util.generate_emb(text, embedding_type="sent")
    #emb = get_embeddings(text, embedding_type="sent")
    print ("Time taken to get Sent Trans embeddings: ", time.time()-a)
    print (type(emb))
    # emb = np.asarray(emb).reshape(-1,1024)
    print (emb.shape)
    
    # gics_pred, gics_proba = _gics_classifier.predict_classes(text) 
    # print(gics_pred, gics_proba)

    if domain:
        #######################
        # Get Li Specialties
        #######################


        specs_and_industry = get_specialties_and_industry(domain)
        # industry = get_specialties(industry)

        if specs_and_industry != {}:


            specialties = specs_and_industry['keywords']
            industry = specs_and_industry['industry']
        
        else:
            common_keywords = []
        
        ######################
        # Get Li co-occuring tags
        ######################

        if specs_and_industry != {}:
            
            co_occurring_tags_list = []

            print("Original specialties tags: ", len(specialties))

            for tag in specialties:
                
                try:

                    co_tag_results = es_cooccur.search_query(settings.LINKEDIN_COOCCUR_NAME_FIELD, tag, size=5)["hits"]["hits"]

                    for result in co_tag_results:

                        co_occurring_tags = []

                        if result["_source"][settings.LINKEDIN_COOCCUR_NAME_FIELD] == tag:
                            
                            # tag_dict = result["_source"][settings.LINKEDIN_COOCCUR_CO_TAGS_FIELD]

                            # tag_list = list(tag_dict.keys())
                            # # tag_count = list(tag_dict.values())
                            # # print(tag_list[:2])
                            # co_occrence_list = np.argsort(list(tag_dict.values()))[::-1]
                            # # print(co_occrence_list[:2])
                            # co_occurring_tags = np.array(tag_list)[co_occrence_list]
                            # # if len(li_specialities)>len(co_occrence_list):
                            # #     co_occurring_tags = [tag_list[i] for i in co_occrence_list if tag_list[i] in li_specialities]
                            # # else:
                            # #     co_occurring_tags = [li_tag for li_tag in li_specialities if tag_list[i] in li_specialities]

                            # # co_occurring_tags = [tag_list[i] for i in co_occrence_list]
                            # print(co_occurring_tags[:3])
                            # # print([tag_count[i] for i in co_occrence_list][:5])
                            # if len(co_occurring_tags)>3:
                            #     co_occurring_tags = co_occurring_tags[:3]
                            
                            # co_occurring_tags = [i for i in co_occurring_tags if i in li_specialities]

                            tag_dict = {"tags": [], "tag_count": []}

                            for key, item in result["_source"][settings.LINKEDIN_COOCCUR_CO_TAGS_FIELD].items():
                                tag_dict["tags"].append(key)
                                tag_dict["tag_count"].append(item)

                            tag_df = pd.DataFrame(tag_dict)

                            tag_df = tag_df.sort_values("tag_count", ascending=False)

                            print("Original co occurence df:\n", tag_df.head())

                            tag_df = tag_df.loc[tag_df["tag_count"]>1]
                            
                            tag_df["inc_falg"] = tag_df.tags.apply(lambda x: x in li_specialities)

                            tag_df = tag_df.loc[tag_df["inc_falg"]]        
                            
                            print("Filtered co occurence df:\n", tag_df.head())

                            co_occurring_tags = tag_df["tags"].tolist()

                            if len(co_occurring_tags)>3:
                                co_occurring_tags = co_occurring_tags[:3]
                            
                
                except Exception as e:

                    print("Tag can not be found in the co occuring elk index: ", tag)
                    print(e)

                    co_occurring_tags = []

                co_occurring_tags_list.extend(co_occurring_tags)

            specialties.extend(list(set(co_occurring_tags_list)))

            print("Additional tags count: ", len(co_occurring_tags_list))
            print("Li occuring specialties tags included: ", len(specialties))

        ######################
        # Find Keywords
        ######################

        if specs_and_industry != {}:

            multilabel_words = []

            for multilabel_tag in multi_label_pred:
                multilabel_words.extend(multilabel_tag.lower().split("_"))

            specialties_words = []

            for specialty in specialties:
                specialties_words.extend(specialty.lower().split(" "))

            
            stop_words = ['and', '&']

            
            
            common_keywords = [multilabel_word for multilabel_word in multilabel_words if multilabel_word in specialties_words and multilabel_word not in stop_words]

            common_keywords = list(set(common_keywords))

            print("Multilabel:", multilabel_words)
            print("Specialties:", specialties_words)
            print("Keywords:", common_keywords)
    else:
        common_keywords = []



    
    predictions = _similarity.predict(
        text, emb, gics_pred,
         common_keywords, country = country,
         requested_mnn=mnn,ml_tags = ml_tags
         )
    #print(predictions)
    

    return predictions


if __name__ == "__main__":

    text =  "Paytm is an online money wallet"
    handle_similarity_request_sent_trans(text)
