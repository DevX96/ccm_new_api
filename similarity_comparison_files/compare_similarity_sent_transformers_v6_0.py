"""Similarity API based on gics_group_dict and manual_group"""

import numpy as np
import pandas as pd

from elasticsearch import Elasticsearch, helpers
from typing import Dict, List, Any
import plyvel
from datetime import datetime

import settings

from text_similarity_utils.distance_functions import compute_cosine_similarity
import copy
from operator import itemgetter
import pickle


class Similarity():

    def __init__(
        self,
        gics_group_dict: Dict[str, Dict[str, List]], 
        es_server: Elasticsearch
        ):
        
        self.gics_group_dict = gics_group_dict

        self.es_server = es_server


    def find_keyword_matches(self, description, keyword_list):

        number_matches = 0

        if keyword_list == []:
            return number_matches

        for keyword in keyword_list:
            if keyword in description:
                number_matches += 1
        
        return number_matches

    def get_embeddings(
        self,
        uuids: List,
        es_index: str,
        uuid_field: str,
        embedding_field: str):
        """Fetch embeddings from elasticsearch index

        Args:
            uuids (List): List of all capiq ids (CTM/CCM)
            es_index (str): Name of elasticsearch index
            uuid_field (str): Name of uuid field in elasticsearch index
            embedding_field (str): Name of embedding field in elasticsearch index

        Returns:
            uuid_arr: [numpy.array]: Numpy array of fetched uuids
            emb_arr: [numpy.array]: Numpy array of fetched embeddings
        """
        get_embeddings_query = {
            "query": {
                "terms": {
                    uuid_field: uuids
                    }
                }
            }

        all_embeddings = helpers.scan(
                self.es_server, 
                index = es_index,
                query = get_embeddings_query,
                scroll="60m"
                )
            
        all_embeddings = [i["_source"] for i in all_embeddings]

        uuid_arr = np.array([i["capiq_uuid"] for i in all_embeddings])
        emb_arr = np.array([i["embedding"] for i in all_embeddings])
        
        return uuid_arr, emb_arr

    def cosine_distance(
        self,
        sub_emb: np.array,
        target_embs: np.array,
        target_uuids: np.array):
        """Calculate cosine distance and return dictionary with 
        key as uuid and value as cosine score

        Args:
            sub_emb (np.array): Sentence transformer embedding of subject company
            target_embs (np.array): Sentence transformer embeddings of targets companies
            target_uuids (np.array): Numpy array of target uuids

        Returns:
            [List: Dict]: List of Dictionary with uuid and cosine score
        """
        cosine_score = compute_cosine_similarity(sub_emb, target_embs)
        score_list = [{
            "uuid": int(target_uuid),
            "score": float(target_score)
        } for target_uuid, target_score in zip(target_uuids, cosine_score)]
        return score_list

    
    def predict(
        self, 
        description: str, 
        embedding: np.array, 
        gics_tag: str, 
        keywords: List,
        country: str = None,
        required_ccm: bool = True,
        required_ctm: bool = True,
        valuation_date: str = None
        ):

        results = {}   


        if required_ccm:
            ####
            # ccm Processing
            ####

            # Fetch results from elasticsearch

            es_query = {
                "size": 200,
                "query":{
                    "bool":{
                        "must":{
                            "match": {
                                settings.CAPIQ_CCM_DESC_FIELD: description
                            }
                        },
                        "filter":{
                            "terms":{
                                settings.CAPIQ_CCM_GICS_FIELD+".keyword": self.gics_group_dict[gics_tag]['manual_group']
                            }
                        }
                    }
                }
            }

            ccm_es_results = helpers.scan(
                self.es_server, 
                index = settings.CAPIQ_CCM_INDEX,
                query = es_query,
                scroll="60m",
                size=200,
                preserve_order=True
                )

            ccm_es_results = [i["_source"] for i in ccm_es_results][:200]
            all_ccm_ids = [i[settings.CAPIQ_CCM_UUID_FIELD] for i in ccm_es_results]

            print("Total companies: ", len(ccm_es_results))

            # Sorting based on market cap
            # ccm_es_results_market_cap = [i for i in ccm_es_results if (i[settings.CAPIQ_CCM_MARKET_CAP_FIELD] != "") and  (settings.CAPIQ_CCM_MARKET_CAP_FIELD in i)]
            ccm_es_results_market_cap = []
            for i in ccm_es_results:
                if settings.CAPIQ_CCM_MARKET_CAP_FIELD in i:
                    if i[settings.CAPIQ_CCM_MARKET_CAP_FIELD] != "":
                        ccm_es_results_market_cap.append(i)

            ccm_es_results_market_cap = sorted(
                ccm_es_results_market_cap, 
                key = lambda x: x[settings.CAPIQ_CCM_MARKET_CAP_FIELD],
                reverse=True
                )

            # top 3 ids based on market cap
            top3_market_cap_ids = []

            if country:
                counter = 0
                for ccm_es_result in ccm_es_results_market_cap:
                    # print(counter)
                    if counter==3:
                        break
                    if ccm_es_result[settings.CAPIQ_CCM_MARKET_COUNTRY_FIELD] == country:
                        top3_market_cap_ids.append(ccm_es_result)
                        ccm_es_results.remove(ccm_es_result)
                        counter += 1
            
            del ccm_es_results_market_cap

            print("Top 3 market cap companies: ", len(top3_market_cap_ids))
            print(" ..Remaining companies: ", len(ccm_es_results))

            # top 5 based on bm25:
            top5_bm25_ids = copy.deepcopy(ccm_es_results[:5])

            ccm_es_results = copy.deepcopy(ccm_es_results[5:])

            print("Top 5 bm25 companies: ", len(top5_bm25_ids))
            print(" ..Remaining companies: ", len(ccm_es_results))

            # Fetch embeddings from elasticsearch
            all_ccm_ids, all_ccm_embeddings = self.get_embeddings(
                all_ccm_ids,
                settings.CAPIQ_CCM_EMB_INDEX,
                settings.CAPIQ_CCM_EMB_UUID_FIELD,
                settings.CAPIQ_CCM_EMB_EMBEDDING_FIELD)
            print("CTM id shape: ", all_ccm_ids.shape)
            print("CTM embs shape: ", all_ccm_embeddings.shape)

            print("Sub Emb Shape:", embedding.shape)
            print("ccm Emb Shape:", all_ccm_embeddings.shape)

            # Calculate cosine distance for all target companies
            dist_list = self.cosine_distance(
                embedding,
                all_ccm_embeddings,
                all_ccm_ids)

            # Fetch Top n companies based on dist list
            dist_dict = {i["uuid"]: i["score"] for i in dist_list}

            dist_in_order_of_ccm_es_result = np.array([dist_dict[i[settings.CAPIQ_CCM_UUID_FIELD]]  if i[settings.CAPIQ_CCM_UUID_FIELD] in dist_dict else 0 for i in ccm_es_results])
            print("ccm Dists Shape", dist_in_order_of_ccm_es_result.shape)
            n_s = int(min(50, dist_in_order_of_ccm_es_result.shape[0]))

            dist_in_order_of_ccm_es_result = dist_in_order_of_ccm_es_result.reshape(1, dist_in_order_of_ccm_es_result.shape[0])

            top_n_ccm = np.argpartition(-dist_in_order_of_ccm_es_result, range(n_s))[:,:n_s]

            print("Top 100 Shape", top_n_ccm.shape)

            ccm_es_results = [ccm_es_results[i] for i in top_n_ccm[0]]

            # Combining market cap, bm25 and embedding
            final_result = top5_bm25_ids + ccm_es_results + top3_market_cap_ids
            print("Final result: ", len(final_result))

            del top3_market_cap_ids
            del top5_bm25_ids
            del ccm_es_results

            # Finalizing CCM result

            ids_keyword_scores = [
                {
                    "uuid": i[settings.CAPIQ_CCM_UUID_FIELD], 
                    "keyword_score": self.find_keyword_matches(i[settings.CAPIQ_CCM_DESC_FIELD], keywords)
                }
                for i in final_result
                ]

            del final_result

            # Sorting based on keyword score
            ids_keyword_scores = sorted(
                ids_keyword_scores, 
                key = lambda x: x["keyword_score"],
                reverse=True
                )

            results["ccm"] = {
                "ids": [int(i["uuid"]) for i in ids_keyword_scores],
                "keyword_matches": [int(i["keyword_score"]) for i in ids_keyword_scores]
            }

            # Calculating cosine score for all ids

            results["ccm"]["distance_score"] = dist_list

        

        if required_ctm:
            ####
            # ctm Processing
            ####

            if valuation_date:
                valuation_date = datetime.strptime(valuation_date, '%Y-%m-%d')
                es_query = {
                    "size": 200,
                    "query":{
                        "bool":{
                            "must":{
                                "match": {
                                    settings.CAPIQ_CTM_DESC_FIELD: description
                                }
                            },
                            "filter": [{
                                "terms":{
                                    settings.CAPIQ_CTM_GICS_FIELD+".keyword": self.gics_group_dict[gics_tag]['manual_group']
                                }
                            },
                            {
                            "range": {
                                    settings.CAPIQ_CTM_TRANSACTION_DATE_FIELD:{
                                        "gte": valuation_date
                                    }
                                }
                            }]
                        }
                    }
                }

            else:
                es_query = {
                    "size": 200,
                    "query":{
                        "bool":{
                            "must":{
                                "match": {
                                    settings.CAPIQ_CTM_DESC_FIELD: description
                                }
                            },
                            "filter":{
                                "terms":{
                                    settings.CAPIQ_CTM_GICS_FIELD+".keyword": self.gics_group_dict[gics_tag]['manual_group']
                                }
                            }
                        }
                    }
                }

            ctm_es_results = helpers.scan(
                self.es_server, 
                index = settings.CAPIQ_CTM_INDEX,
                query = es_query,
                scroll="60m",
                size=200,
                preserve_order=True
                )

            ctm_es_results = [i["_source"] for i in ctm_es_results][:200]
            all_ctm_ids = [i[settings.CAPIQ_CTM_UUID_FIELD] for i in ctm_es_results]

            print("Total companies: ", len(ctm_es_results))

            # top 5 based on bm25:
            top5_bm25_ids = copy.deepcopy(ctm_es_results[:5])

            ctm_es_results = copy.deepcopy(ctm_es_results[5:])

            print("Top 5 bm25 companies: ", len(top5_bm25_ids))
            print(" ..Remaining companies: ", len(ctm_es_results))
            
            # Fetch embeddings from elasticsearch
            all_ctm_ids, all_ctm_embeddings = self.get_embeddings(
                all_ctm_ids,
                settings.CAPIQ_CTM_EMB_INDEX,
                settings.CAPIQ_CTM_EMB_UUID_FIELD,
                settings.CAPIQ_CTM_EMB_EMBEDDING_FIELD)
            print("CTM id shape: ", all_ctm_ids.shape)
            print("CTM embs shape: ", all_ctm_embeddings.shape)

            print("Sub Emb Shape:", embedding.shape)
            print("ccm Emb Shape:", all_ctm_embeddings.shape)

            # Calculate cosine distance for all target companies
            dist_list = self.cosine_distance(
                embedding,
                all_ctm_embeddings,
                all_ctm_ids)

            # Fetch Top n companies based on dist list
            dist_dict = {i["uuid"]: i["score"] for i in dist_list}

            dist_in_order_of_ctm_es_result = np.array([dist_dict[i[settings.CAPIQ_CTM_UUID_FIELD]]  if i[settings.CAPIQ_CTM_UUID_FIELD] in dist_dict else 0 for i in ctm_es_results])
            print("ccm Dists Shape", dist_in_order_of_ctm_es_result.shape)
            n_s = int(min(50, dist_in_order_of_ctm_es_result.shape[0]))

            dist_in_order_of_ctm_es_result = dist_in_order_of_ctm_es_result.reshape(1, dist_in_order_of_ctm_es_result.shape[0])

            top_n_ctm = np.argpartition(-dist_in_order_of_ctm_es_result, range(n_s))[:,:n_s]

            print("Top 100 Shape", top_n_ctm.shape)

            ctm_es_results = [ctm_es_results[i] for i in top_n_ctm[0]]

            final_result = top5_bm25_ids + ctm_es_results
            print("Final result: ", len(final_result))

            del top5_bm25_ids
            del ctm_es_results

            # Finalizing CTM result

            ids_keyword_scores = [
                {
                    "uuid": i[settings.CAPIQ_CTM_UUID_FIELD], 
                    "keyword_score": self.find_keyword_matches(i[settings.CAPIQ_CTM_DESC_FIELD], keywords)
                }
                for i in final_result
                ]

            del final_result

            # Sorting based on keyword score
            ids_keyword_scores = sorted(
                ids_keyword_scores, 
                key = lambda x: x["keyword_score"],
                reverse=True
                )

            results["ctm"] = {
                "ids": [int(i["uuid"]) for i in ids_keyword_scores],
                "keyword_matches": [int(i["keyword_score"]) for i in ids_keyword_scores]
            }

            # Calculating cosine score for all ids

            results["ctm"]["distance_score"] = dist_list

        return results



if __name__ == "__main__":

    import time
    import sys
    from embeddings_sentence_transformers import Embeddings
    from get_gics4_tags import GetGics4Tags

    gics_classes_path = "/mnt/gics4_model/gics4_tag_list.npy"
    gics_weights_path = "/mnt/gics4_model/weights/weights-improvement-64-1.23.hdf5"

    get_gics = GetGics4Tags(classes_path=gics_classes_path, weight_path=gics_weights_path)

    emb_generator = Embeddings()

    tic_l = time.time()

    gics_group_dict = np.load(settings.GICS_L4_GROUP_PATH, allow_pickle=True).item()

    elk_search = Elasticsearch(['23.102.26.210'], 
                        http_auth=("elastic", \
                            "73Elk@Uar2020"),
                        scheme="http",
                        port=9200,
                        retry_on_timeout=True)

    similarity = Similarity(settings.CAPIQ_CCM_SENTENCE_TRANSFORMERS_EMBS_PATH, settings.CAPIQ_CCM_SENTENCE_TRANSFORMERS_UUID_PATH, gics_group_dict, elk_search)

    print("Time Taken to Load All Datasets", time.time() - tic_l)


    companies = 'Stripe'

    description = 'Stripe is a global technology company that builds economic infrastructure for the internet. Businesses of every sizefrom new startups to public companies like Salesforce and \
    Facebookuse the companys software to accept online payments and run complex global operations. Today millions of companies across more than 100 countries use Stripe to start, run, and scale their\
    businesses online. Stripe combines economic infrastructure with a set of applications for new business models like crowdfunding and marketplaces, fraud prevention, analytics, and more. As the \
    worlds fastest-advancing developer platform, Stripe navigates global regulatory uncertainty and partners closely with internet leaders like Apple, Google, Alipay, Tencent, Facebook, Twitter to \
    launch new capabilities. Only about three percent of GDP happens online today. Stripe wants to help more companies get started and thrive, and ultimately to grow the GDP of the internet.'


    print("Doing Analysis for Company: ", companies[i])


    embedding = emb_generator.get_embeddings(description)
    print(embedding.shape)

    t0 = time.time()
    gics_tag, gics_proba = get_gics.predict_classes(description)

    print(gics_tag, gics_proba)

    tic = time.time()
    results = similarity.predict(description, embedding, gics_tag=gics_tag, keywords=[])

    print(tic - t0, "seconds")
    print(time.time() - tic, "seconds")

    print(results)
    print("Length", len(results['ccm']))

    print(results)
        