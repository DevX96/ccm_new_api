import numpy as np
import pandas as pd
import json
from elasticsearch import Elasticsearch, helpers
from typing import Dict, List, Any
from multilabel_competitors import MNNSimilarity

from datetime import datetime
import settings
import time

from text_similarity_utils.distance_functions import compute_cosine_similarity
import copy
from operator import itemgetter



class Similarity():

    def __init__(
        self, 
        ccm_emb: str, 
        ccm_ticker: str, 
        gics_group_dict: Dict[str, Dict[str, List]], 
        es_server: Elasticsearch
        ):
        
        self.ccm_emb = np.load(ccm_emb)
        self.ccm_ticker = np.load(ccm_ticker)

        self.gics_group_dict = gics_group_dict

        self.es_server = es_server

        self.ind_gics_map = json.load(open(settings.IND_GICS_MAP_PATH))

    def get_description_from_id(self,company_id):
        
        es_query = {
                        "query":{
                            "match":{
                                "company_id": company_id
                            }
                        }
                    }
        es_results = self.es_server.search(index="company_master_data_with_multilabel_classification_v2", body=es_query, size = 1)["hits"]["hits"]

        if es_results:
            company_description = es_results[0]["_source"][settings.CAPIQ_CCM_DESC_FIELD]
        else:
            company_description = " "

        return company_description

    def find_keyword_matches(self, description, keyword_list):

        number_matches = 0

        if keyword_list == []:
            return number_matches

        for keyword in keyword_list:
            if keyword in description:
                number_matches += 1
        
        return number_matches

    def get_embeddings(
        self,
        uuids: List,
        es_index: str,
        uuid_field: str,
        embedding_field: str):
        """Fetch embeddings from elasticsearch index

        Args:
            uuids (List): List of all capiq ids (CTM/CCM)
            es_index (str): Name of elasticsearch index
            uuid_field (str): Name of uuid field in elasticsearch index
            embedding_field (str): Name of embedding field in elasticsearch index

        Returns:
            uuid_arr: [numpy.array]: Numpy array of fetched uuids
            emb_arr: [numpy.array]: Numpy array of fetched embeddings
        """
        print('-'*50)
        print(es_index)
        get_embeddings_query = {
            "query": {
                "terms": {
                    uuid_field: uuids
                    }
                }
            }

        all_embeddings = helpers.scan(
                self.es_server, 
                index = es_index,
                query = get_embeddings_query,
                scroll="60m"
                )
            
        all_embeddings = [i["_source"] for i in all_embeddings]

        uuid_arr = np.array([i["capiq_uuid"] for i in all_embeddings])
        emb_arr = np.array([i["embedding"] for i in all_embeddings])
        
        return uuid_arr, emb_arr

    def cosine_distance(
        self,
        sub_emb: np.array,
        target_embs: np.array,
        target_uuids: np.array):
        """Calculate cosine distance and return dictionary with 
        key as uuid and value as cosine score

        Args:
            sub_emb (np.array): Sentence transformer embedding of subject company
            target_embs (np.array): Sentence transformer embeddings of targets companies
            target_uuids (np.array): Numpy array of target uuids

        Returns:
            [List: Dict]: List of Dictionary with uuid and cosine score
        """
        cosine_score = compute_cosine_similarity(sub_emb, target_embs)
        score_list = [{
            "uuid": int(target_uuid),
            "score": float(target_score)
        } for target_uuid, target_score in zip(target_uuids, cosine_score)]
        return score_list




    
    def predict(
        self, 
        description: str, 
        embedding: np.array, 
        gics_tag: str, 
        keywords: List,
        country: str = None,
        requested_mnn: bool = True,
        requested_ccm: bool = True,
        requested_ctm: bool = True,
        ml_tags = None
        ):

        results = {}   
        
        if requested_ccm:
            ####
            # ccm Processing
            ####

            ################################TOP 5 RESULTS FROM BM25 COMPETITORS ###########################
            
            bm25_es_query = {
                        "query":{
                            "match":{
                                "description": description
                            }
                        }
                    }
        
            bm25_results = self.self.es_server.search(index=settings.CAPIQ_CCM_INDEX, body=bm25_es_query, size = 200)["hits"]["hits"]
            comp_name_list = []
            top5_bm25 = [i["_source"] for i in bm25_results][:5]
            top5_bm25_idxs = [{"uuid":i["company_id"]} for i in top5_bm25]
            

            ################################TOP 200 RESULTS FROM BM25 + GICS COMPETITORS ###########################

            bm25_gics_es_query = {
                "size": 200,
                "query":{
                    "bool":{
                        "must":{
                            "match": {
                                "description": description
                            }
                        },
                        "filter":{
                            "terms":{
                                #settings.CAPIQ_CCM_GICS_FIELD+".keyword": self.gics_group_dict[gics_tag]['manual_group']
                                "industry_classification.keyword": [self.ind_gics_map[a] for a in  self.gics_group_dict[gics_tag]]
                            }
                        }
                    }
                }
            }                            
           
            ccm_es_results = self.es_server.search(index=settings.CAPIQ_CCM_INDEX, body=bm25_gics_es_query, size = 500)["hits"]["hits"]

            ccm_es_results = [i["_source"] for i in ccm_es_results][:200]
            
            all_ccm_ids = [i[settings.CAPIQ_CCM_UUID_FIELD] for i in ccm_es_results]

            ################################ TOP 200 RESULTS FROM MULTI LABEL TAGS ###########################
            
            if requested_mnn:
                print('Fetching mnn results gics')
                start = time.time()    

                mnn_competitors_gics = MNNSimilarity(fetch = False)
                mnn_results_gics = mnn_competitors_gics.predict(description,ccm_es_results.copy(),ml_tags)
                print('Fetched mnn results gics')
                end = time.time()    
                print(f"Time taken for MNN results {end-start:.3f} secs")
                top_25_mnn =  mnn_results_gics
            

            print("Total companies: ", len(ccm_es_results))

            ################################TOP 3 RESULTS FROM MARKET CAP ###########################

            market_cap_es_query = {
                "query":{
                    "bool":{
                        "must":{
                            "match": {
                                "description": description
                            }
                        },
                        "filter":{
                            "terms":{
                                #settings.CAPIQ_CCM_GICS_FIELD+".keyword": self.gics_group_dict[gics_tag]['manual_group']
                                #
                                "industry_classification.keyword": self.ind_gics_map[gics_tag],
                                "country.keyword":country
                            }
                        }
                    }
                }
            }                              
           
            market_cap_es_results = self.es_server.search(index=settings.CAPIQ_CCM_INDEX, body=market_cap_es_query, size = 500)["hits"]["hits"]

            market_cap_es_results = [i["_source"] for i in ccm_es_results]

            # Sorting based on market cap
            ccm_es_results_market_cap = [i for i in market_cap_es_results if i[settings.CAPIQ_CCM_MARKET_CAP_FIELD] != ""]
            ccm_es_results_market_cap = sorted(
                ccm_es_results_market_cap, 
                key = lambda x: x[settings.CAPIQ_CCM_MARKET_CAP_FIELD],
                reverse=True
                )

            # top 3 ids based on market cap
            top3_market_cap_ids = []

            if country:
                counter = 0
                for market_cap_es_result in ccm_es_results_market_cap:
                    # print(counter)
                    if counter==3:
                        break
                    if market_cap_es_results[settings.CAPIQ_CCM_MARKET_COUNTRY_FIELD] == country:
                        top3_market_cap_ids.append(market_cap_es_results)
                        ccm_es_results.remove(market_cap_es_results)
                        counter += 1
            
            del ccm_es_results_market_cap
            del market_cap_es_results

            print("Top 3 market cap companies: ", len(top3_market_cap_ids))
            #print(" ..Remaining companies: ", len(ccm_es_results))

            ################################ TOP 200 RESULTS FROM SENTENCE TRANSFORMERS ###########################

            # Fetch embeddings from elasticsearch
            all_ccm_ids, all_ccm_embeddings = self.get_embeddings(
                all_ccm_ids,
                settings.CAPIQ_CCM_EMB_INDEX,
                settings.CAPIQ_CCM_EMB_UUID_FIELD,
                settings.CAPIQ_CCM_EMB_EMBEDDING_FIELD)
            print("CTM id shape: ", all_ccm_ids.shape)
            print("CTM embs shape: ", all_ccm_embeddings.shape)

            print("Sub Emb Shape:", embedding.shape)
            print("ccm Emb Shape:", all_ccm_embeddings.shape)

            # Calculate cosine distance for all target companies
            dist_list = self.cosine_distance(
                embedding,
                all_ccm_embeddings,
                all_ccm_ids)

            # Fetch Top n companies based on dist list
            dist_dict = {i["uuid"]: i["score"] for i in dist_list}


            dist_dict_sorted = dict(sorted(
                dist_dict.items(), 
                key = lambda x: x[1],
                reverse=True
                ))
            #print(dist_dict_sorted)
            
            dist_list_sorted = [{
            "uuid": int(k),
            "score": float(v)
                } for k,v in dist_dict_sorted.items()]
            
            market_cap_len = len(top3_market_cap_ids)

            ################################ COMBINING RESULTS FROM ABOVE METHODS ###########################

            final_res = {}
            if market_cap_len:
                final_res["ccm"] = {
                    "ids": [i["company_id"] for i in top3_market_cap_ids],
                    "keyword_matches": [0 for i in top3_market_cap_ids]
                    }
                print(final_res['ccm'])
            else:
                final_res['ccm'] = {'ids':[],"keyword_matches":[]}
                
            print([a['company_id'] for a in top3_market_cap_ids])
            
            if requested_mnn:
                
                print('YES')
                for mod,cnt,pr in zip([top5_bm25_idxs,dist_list_sorted,top_25_mnn],[5,40,10],['bm25','sent','mnn'] ):
                    c = 0
                    for res in mod:
                        if c < cnt:
                            
                            if res["uuid"] not in final_res['ccm']['ids']:
                                                               
                                final_res['ccm']['ids'].append(res["uuid"])                            
                                final_res['ccm']['keyword_matches'].append(0)
                                
                                c +=1
                    print(f'Count from {pr} is {c}')
                    
                if len(final_res['ccm']['ids']) - market_cap_len < 55:
                    for res in dist_list_sorted[40:75]:
                        print(1)
                        if len(final_res['ccm']['ids']) - market_cap_len >= 55:
                            break
                        elif res['uuid'] not in final_res['ccm']['ids']:
                            
                            final_res['ccm']['ids'].append(res["uuid"])                            
                            final_res['ccm']['keyword_matches'].append(0)

                
                print('RES  ',len(final_res['ccm']['ids']))


                final_res["ccm"]["distance_score"] = dist_list_sorted            
                final_res['ccm']["mnn_gics"] =  mnn_results_gics

            else:
                print('NO')
                for mod,cnt,pr in zip([top5_bm25_idxs,dist_list_sorted],[5,50],['bm25','sent'] ):
                    c = 0
                    for res in mod:
                        if c < cnt:
                            if res["uuid"] not in final_res['ccm']['ids']:
                                                               
                                final_res['ccm']['ids'].append(res["uuid"])                            
                                #final_res['ccm']['keyword_matches'].append( self.find_keyword_matches(self.get_description_from_id(res["uuid"]),keywords))
                                final_res['ccm']['keyword_matches'].append(0)
                                
                                c +=1
                    print(f'Count from {pr} is {c}')
                if len(final_res['ccm']['ids']) - market_cap_len < 55:
                    for res in dist_list_sorted[50:75]:
                        print(1)
                        if len(final_res['ccm']['ids']) - market_cap_len >= 55:
                            break
                        elif res['uuid'] not in final_res['ccm']['ids']:
                            
                            final_res['ccm']['ids'].append(res["uuid"])                            
                            final_res['ccm']['keyword_matches'].append(0)

                
                print('RES  ',len(final_res['ccm']['ids']))

            
            
            final_res["ccm"]["distance_score"] = dist_list_sorted

        if requested_ctm:
            ####
            # ccm Processing
            ####

            ####
            # ccm Processing
            ####

            # Fetch results from elasticsearch

            es_query = {
                "size": 200,
                "query":{
                    "bool":{
                        "must":{
                            "match": {
                                "description": description
                                #settings.CAPIQ_CTM_DESC_FIELD: description
                            }
                        },
                        "filter":{
                            "terms":{
                                "industry_classification.keyword": [self.ind_gics_map[a] for a in  self.gics_group_dict[gics_tag]]
                                #settings.CAPIQ_CTM_GICS_FIELD+".keyword": self.gics_group_dict[gics_tag]['manual_group']
                            }
                        }
                    }
                }
            }

            #ccm_es_results = helpers.scan(
            #    self.es_server, 
            #    index = settings.CAPIQ_CCM_INDEX,
            #    query = es_query,
            #    scroll="60m",
            #    size=200
            #    )
            ccm_es_results = self.es_server.search(index=settings.CAPIQ_CCM_INDEX, body=es_query, size = 500)["hits"]["hits"]

            ccm_es_results = [i["_source"] for i in ccm_es_results][:200]

            print("Total companies: ", len(ccm_es_results))

        #return results
        return final_res



if __name__ == "__main__":

    import time
    import sys
    from embeddings_sentence_transformers import Embeddings
    from get_gics4_tags import GetGics4Tags

    gics_classes_path = "/mnt/gics4_model/gics4_tag_list.npy"
    gics_weights_path = "/mnt/gics4_model/weights/weights-improvement-64-1.23.hdf5"

    get_gics = GetGics4Tags(classes_path=gics_classes_path, weight_path=gics_weights_path)

    emb_generator = Embeddings()

    tic_l = time.time()

    gics_group_dict = np.load(settings.GICS_L4_GROUP_PATH, allow_pickle=True).item()

    elk_search = Elasticsearch(['23.102.26.210'], 
                        http_auth=("elastic", \
                            "73Elk@Uar2020"),
                        scheme="http",
                        port=9200,
                        retry_on_timeout=True)

    similarity = Similarity(settings.CAPIQ_CCM_SENTENCE_TRANSFORMERS_EMBS_PATH, settings.CAPIQ_CCM_SENTENCE_TRANSFORMERS_UUID_PATH, gics_group_dict, elk_search)

    print("Time Taken to Load All Datasets", time.time() - tic_l)


    companies = 'Stripe'

    description = 'Stripe is a global technology company that builds economic infrastructure for the internet. Businesses of every sizefrom new startups to public companies like Salesforce and \
    Facebookuse the companys software to accept online payments and run complex global operations. Today millions of companies across more than 100 countries use Stripe to start, run, and scale their\
    businesses online. Stripe combines economic infrastructure with a set of applications for new business models like crowdfunding and marketplaces, fraud prevention, analytics, and more. As the \
    worlds fastest-advancing developer platform, Stripe navigates global regulatory uncertainty and partners closely with internet leaders like Apple, Google, Alipay, Tencent, Facebook, Twitter to \
    launch new capabilities. Only about three percent of GDP happens online today. Stripe wants to help more companies get started and thrive, and ultimately to grow the GDP of the internet.'


    print("Doing Analysis for Company: ", companies[i])


    embedding = emb_generator.get_embeddings(description)
    print(embedding.shape)

    t0 = time.time()
    gics_tag, gics_proba = get_gics.predict_classes(description)

    print(gics_tag, gics_proba)

    tic = time.time()
    results = similarity.predict(description, embedding, gics_tag=gics_tag, keywords=[])

    print(tic - t0, "seconds")
    print(time.time() - tic, "seconds")

    print(results)
    print("Length", len(results['ccm']))

    print(results)
        