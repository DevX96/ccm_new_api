import pandas as pd
import requests
import json
import ast

from elasticsearch import Elasticsearch
import numpy as np


class MNNSimilarity():

    def __init__(self,fetch=True):

        self.elk_search = Elasticsearch(['23.102.26.210'], 
                        http_auth=("elastic", 
                            "73Elk@Uar2020"),
                        scheme="http",
                        port=9200,
                        retry_on_timeout=True)

        self.fetch = fetch
        
        self.es_index = "company_master_data_with_multilabel_classification_v2"
        self.secret_key = "@P@>w]128-.>12]'jRq120y|ap.:HF.)Bqawetgagbfshqwetqegh"
        self.api_ip = "http://0.0.0.0:8084/"
        self.api_route = "multi_label_classification"


    def find_es_rank(self,subject_description):

        es_query = {
                        "query":{
                            "match":{
                                "description": subject_description
                            }
                        }
                    }
        
        es_results = self.elk_search.search(index=self.es_index, body=es_query, size = 200)["hits"]["hits"]
        comp_name_list = []

        print("Number of ES Results", len(es_results))


        es_results = [i["_source"] for i in es_results][:200]

        return es_results
    
    def find_multilabel_tags_from_es(self,search_query):

        es_query = {
                        "query":{
                            "match":{
                                "company_id": search_query
                            }
                        }
                    }
        
        es_results = self.elk_search.search(index=self.es_index, body=es_query, size = 1)["hits"]["hits"]
        if es_results:
            ml_dict = es_results[0]["_source"]["multilabel_classification_tags"]
        
        ml_list = list(ml_dict)

        return ml_list,ml_dict

    def find_multilabel_tags(self,search_query):

        api_request = {
        "secret_id":self.secret_key,
        "request_id":"ce542f35-3df9-457c-a68e-0382e72364ae",
        "data":[{
            "id":"id",
            "text":search_query
        }]       
        }
        api_response = requests.post(self.api_ip+self.api_route, json=api_request).json()
        
        response = list(api_response['complete_processed_data'][0]['processed_data'])
        #print(response)
        ml_dict = {}
        ml_list = []
        for tag,val in response:
            ml_dict[tag] = round(float(val),3)

        ml_list = list(ml_dict)

        #ml_tags = [i for i,_ in response ]



        return ml_list,ml_dict

    def get_target_match_dict(self,subject_company_ml_tags,ml_tags,ml_dict):

        match_dict_ord = {}
        matches_dict = {0:'I',1:"II",2:"III",3:"IV",4:"V"}
        matches_dict_score = {}
        match_dict_ord_pr = {}
        priority_match = {'priority_match':False,"count":0 }

        for i,tag_ in enumerate(subject_company_ml_tags):
            if tag_ in ml_tags:
                match_dict_ord[tag_] = ml_tags.index(tag_) + 1
                match_dict_ord_pr[matches_dict[i]] = ml_tags.index(tag_) + 1
                matches_dict_score[matches_dict[i]] = ml_dict[tag_]

        try:
            if match_dict_ord_pr["I"]:
                priority_match['priority_match'] = True
        
        except:
            pass

        priority_match['count'] = len(match_dict_ord_pr)
    
        return match_dict_ord_pr,match_dict_ord,priority_match,matches_dict_score

    
    def calculate_multilabel_score(self,priority_match,matches_dict_score = None):
        
        if not matches_dict_score:
            score = 0
            if priority_match['priority_match']:
                score += 10
            
            score += int(priority_match['count'])

            return score
        else:
            score = 0
            for a in matches_dict_score:
                if a == 'I':
                    score += matches_dict_score[a] * 10
                else:
                    score += matches_dict_score[a]
            return score

    def calculate_multilabel_score_v2(self,priority_match,matches_dict_score = None):
        
        if not matches_dict_score:
            score = 0
            if priority_match['priority_match']:
                score += 10
            
            score += int(priority_match['count'])

            return score
        else:
            score = 0
            for a in matches_dict_score:
                if a == 'I':
                    score += matches_dict_score[a] * 10
                else:
                    score += matches_dict_score[a]
            return score
    

    
    def predict(self,subject_description,es_results = None,ml_tags = None):
        print(self.es_index)
        if ml_tags:
            subject_company_ml_tags = ml_tags
        else:
            subject_company_ml_tags,_ = self.find_multilabel_tags(subject_description)
            print(subject_company_ml_tags)

        if self.fetch:
            es_results = self.find_es_rank(subject_description)
        

        #new_res = []
        for res in es_results:
            ml_tags,ml_dict = self.find_multilabel_tags_from_es(res['company_id'])
            #new_res['company_id'] = res['company_id']

            res['multilabel_tags'] = ml_tags
            _,_,priority_match,matches_dict_score = self.get_target_match_dict(subject_company_ml_tags,ml_tags,ml_dict)

            res['ml_score']  = self.calculate_multilabel_score_v2(priority_match)
            

        sorted_es_results = list(sorted(es_results, key=lambda k: k['ml_score'],reverse = True))

        idx_scores = [
            {
                "uuid":i["company_id"],
                "ml_score":i['ml_score']
            }
            for i in sorted_es_results if i['ml_score'] > 3
        ]
        print('Num of ml competitors ',len(idx_scores))
        
        return idx_scores

