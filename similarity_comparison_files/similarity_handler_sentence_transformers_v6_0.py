from compare_similarity_sent_transformers_v6_0 import Similarity
from elastic_search.elastic_search_processing import ElasticServer
from apollo_api import get_specialties_and_industry
from text_similarity_utils.text_similarity_utils import TextSimilarityUtils
# from embeddings_generator.embeddings_request import get_embeddings
import numpy as np
import pandas as pd
import time
import settings
from datetime import datetime


with open(settings.LINKEDIN_HIGH_OCCRENRRENCE_SPECIALTIES_PATH) as li_specialities:

    li_specialities = li_specialities.readlines()

li_specialities = [i.replace("\n", "").strip() for i in li_specialities]


def handle_similarity_request_sent_trans_v6_0(text, domain, multilabel_classifier, elk_server, li_server, gics_groups_dict, sent=True, cap=True, gics_pred=None, country = None, valuation_date = None):

    es_cooccur = ElasticServer(li_server, index=settings.LINKEDIN_COOCCUR_INDEX)

    _similarity = Similarity(gics_groups_dict, elk_server)

    ts_util = TextSimilarityUtils(text)

    ######################
    # Get BERT embeddings and Multilabel Tags
    ######################

    a = time.time()
    bert_emb = ts_util.generate_emb(text, embedding_type="bert")
    print ("Time taken to get BERT embeddings: ", time.time()-a)
    print (type(bert_emb))
    # emb = np.asarray(emb).reshape(-1,1024)
    print (bert_emb.shape)

    new_multilabel_tags = multilabel_classifier.predict_classes(bert_emb, batch_size=1, top_n=5)[0][0].tolist()

    ######################
    # Get Sentence Transformer Embeddings
    ######################

    a = time.time()
    emb = ts_util.generate_emb(text, embedding_type="sent")
    print ("Time taken to get Sent Trans embeddings: ", time.time()-a)
    print (type(emb))
    # emb = np.asarray(emb).reshape(-1,1024)
    print (emb.shape)

    #######################
    # Get Li Specialties
    #######################

    if domain:
        specs_and_industry = get_specialties_and_industry(domain)

        if specs_and_industry != {}:


            specialties = specs_and_industry['keywords']
            industry = specs_and_industry['industry']
        
        else:
            common_keywords = []
        
        ######################
        # Get Li co-occuring tags
        ######################

        if specs_and_industry != {}:
            
            co_occurring_tags_list = []

            print("Original specialties tags: ", len(specialties))

            for tag in specialties:
                
                try:

                    co_tag_results = es_cooccur.search_query(settings.LINKEDIN_COOCCUR_NAME_FIELD, tag, size=5)["hits"]["hits"]

                    for result in co_tag_results:

                        co_occurring_tags = []

                        if result["_source"][settings.LINKEDIN_COOCCUR_NAME_FIELD] == tag:

                            tag_dict = {"tags": [], "tag_count": []}

                            for key, item in result["_source"][settings.LINKEDIN_COOCCUR_CO_TAGS_FIELD].items():
                                tag_dict["tags"].append(key)
                                tag_dict["tag_count"].append(item)

                            tag_df = pd.DataFrame(tag_dict)

                            tag_df = tag_df.sort_values("tag_count", ascending=False)

                            print("Original co occurence df:\n", tag_df.head())

                            tag_df = tag_df.loc[tag_df["tag_count"]>1]
                            
                            tag_df["inc_falg"] = tag_df.tags.apply(lambda x: x in li_specialities)

                            tag_df = tag_df.loc[tag_df["inc_falg"]]        
                            
                            print("Filtered co occurence df:\n", tag_df.head())

                            co_occurring_tags = tag_df["tags"].tolist()

                            if len(co_occurring_tags)>3:
                                co_occurring_tags = co_occurring_tags[:3]
                            
                
                except Exception as e:

                    print("Tag can not be found in the co occuring elk index: ", tag)
                    print(e)

                    co_occurring_tags = []

                co_occurring_tags_list.extend(co_occurring_tags)

            specialties.extend(list(set(co_occurring_tags_list)))

            print("Additional tags count: ", len(co_occurring_tags_list))
            print("Li occuring specialties tags included: ", len(specialties))

        ######################
        # Find Keywords
        ######################

        if specs_and_industry != {}:

            multilabel_words = []

            for multilabel_tag in new_multilabel_tags:
                multilabel_words.extend(multilabel_tag.lower().split("_"))

            specialties_words = []

            for specialty in specialties:
                specialties_words.extend(specialty.lower().split(" "))

            
            stop_words = ['and', '&']

            
            
            common_keywords = [multilabel_word for multilabel_word in multilabel_words if multilabel_word in specialties_words and multilabel_word not in stop_words]

            common_keywords = list(set(common_keywords))

            print("Multilabel:", multilabel_words)
            print("Specialties:", specialties_words)
            print("Keywords:", common_keywords)
    else:
        common_keywords = []



    
    predictions = _similarity.predict(
        text, 
        emb, 
        gics_pred, 
        common_keywords, 
        country = country, 
        required_ccm=sent, 
        required_ctm=cap,
        valuation_date=valuation_date
        )
    
    print(predictions)

    return predictions


if __name__ == "__main__":

    text =  "Paytm is an online money wallet"
    handle_similarity_request_sent_trans(text)
